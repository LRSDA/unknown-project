@extends('layouts.menu')

@section('title', 'Daftar User')

@section('header', 'User')

@section('create', route('user.create'))

@section('content')
	<div class="mb-3 form-inline">
		<div class="form-group mr-2">
			<input class="form-control searchClue" type="text">
		</div>
		<button class="btn btn-primary" onclick="search(1);"> Search </button>
	</div>
	<table class="table table-sm table-striped table-bordered table-dark text-center">
		<thead class="thead-light">
			<tr>
				<th scope="col" class="align-middle">#</th>
				<th scope="col" class="align-middle">Nama</th>
				<th scope="col" class="align-middle">Role</th>
				<th scope="col" class="align-middle">No. ID</th>
				<th scope="col" class="align-middle">No. Telepon</th>
				<th scope="col" class="align-middle">Alamat</th>
				<th scope="col" class="align-middle">No. Branch</th>
				<th scope="col" class="align-middle">Aksi</th>
			</tr>
		</thead>
		<tbody class="implementSearch">
			@foreach($users as $user)
				<tr>
					<th scope="row" class="align-middle" rowspan="2">{{ $user->id }}</th>
					<td class="align-middle search" name="{{ $user->name }}">{{ $user->name }}</td>
					<td class="align-middle search" name="{{ $user->role }}">{{ $user->role }}</td>
					<td class="align-middle" rowspan="2">{{ $user->identity_number }}</td>
					@if(empty($user->telephone))
						<td class="align-middle" rowspan="2">-</td>
					@else
						<td class="align-middle" rowspan="2">{{ $user->telephone }}</td>
					@endif
					@if(empty($user->address))
						<td class="align-middle" rowspan="2">-</td>
					@else
						<td class="align-middle" rowspan="2">{{ $user->address }}</td>
					@endempty
					@if(empty($user->branch_id))
						<td class="align-middle" rowspan="2">-</td>
					@else
						<td class="align-middle" rowspan="2">@php echo is_null($user->branch_id) ? 0 : $branch[$user->branch_id]; @endphp</td>
					@endif
					<td class="align-middle" rowspan="2">
						<a class="btn btn-info btn-sm my-1" href="{{ route('user.edit', $user->id) }}" role="button">Ubah</a>
						<form method="POST" action="{{ route('user.destroy', $user->id) }}">
							@method('DELETE')
							@csrf
							<input type="submit" class="btn btn-danger btn-sm my-1" value="Hapus">
						</form>
					</td>
				</tr>
				<tr>
					<td class="align-middle">{{ $user->email }}</td>
					<td class="align-middle">{{ $user->status }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection