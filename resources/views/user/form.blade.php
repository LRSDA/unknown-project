<div class="p-4 rounded bg-light">
	<h3>@yield('title')</h3>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="username">Nama</label>
			<input class="form-control" id="username" name="username" maxlength="10" placeholder="Maksimal 10 karakter" 
				@hasSection('value_name')
					value="@yield('value_name')"
				@endif
			>
		</div>
		<div class="form-group col-lg-6">
			<label for="email">Email</label>
			<input class="form-control" id="email" name="email" type="email" maxlength="30" 
				@hasSection('value_email')
					value=@yield('value_email')
				@endif
			>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="password">Password</label>
			<input class="form-control" id="password" name="password" type="password" maxlength="15" placeholder="Kombinasi angka, huruf dan karakter khusus">
		</div>
		<div class="form-group col-lg-6">
			<label for="re-password">Konfirmasi Password</label>
			<input class="form-control" id="re-password" name="password_confirmation" type="password" maxlength="30" placeholder="Masukkan kembali password">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-8">
			<label for="role">Role</label>
			<select class="form-control" id="role" name="role">
				@foreach($roles as $role)
					@hasSection('value_role')
						@if($user->role == $role)
							<option value="{{ $role }}" selected="true">{{ $role }}</option>
						@else
							<option value="{{ $role }}">{{ $role }}</option>
						@endif
					@else
						<option value="{{ $role }}">{{ $role }}</option>
					@endif
				@endforeach
			</select>
		</div>
		<div class="form-group col-lg-4">
			<label for="user_status">Status</label>
			<select class="form-control" id="user_status" name="user_status"
				@hasSection('value_status')
					value=@yield('value_status')
				@endif>
				<option value="Active">Active</option>
				<option value="inactive">Inactive</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-4">
			<label for="identity_number">No. Identitas</label>
			<input class="form-control" type="number" id="identity_number" name="identity_number" maxlength="16" placeholder="Maksimal 16 angka" 
				@hasSection('value_noid')
					value=@yield('value_noid')
				@endif
			>
		</div>
		<div class="form-group col-lg-4">
			<label for="user_tel">No. Telepon</label>
			<input class="form-control" id="user_tel" type="tel" name="user_tel" maxlength="15" placeholder="Maksimal 15 digit" 
				@hasSection('value_tel')
					value=@yield('value_tel')
				@endif
			>
		</div>
		<div class="form-group col-lg-4">
			<label for="branch_id">Branch</label>
			<!-- <input class="form-control" id="branch_id" type="number" name="branch_id" min="1" 
				@hasSection('value_branch')
					value=@yield('value_branch')
				@endif
			> -->
			<select class="form-control" id="branch_id" name="branch_id" >
				@foreach($branches as $branch)
					@hasSection('value_branch')
						@if($user->branch_id == $branch->id)
							<option value="{{ $branch->id }}" selected="true">{{ $branch->name }}</option>
						@else
							<option value="{{ $branch->id }}">{{ $branch->name }}</option>
						@endif
					@else
						<option value="{{ $branch->id }}">{{ $branch->name }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="address">Alamat</label>
		<textarea class="form-control" id="address" name="address" maxlength="100" placeholder="Maksimal 100 karakter">@hasSection('value_addr')@yield('value_addr')@endif</textarea>
	</div>
	<input type="submit" class="btn btn-success" value="Unggah">
	<input type="reset" class="btn btn-secondary" value="Reset">
</div>