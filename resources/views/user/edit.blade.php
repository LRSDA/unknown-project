@extends('layouts.menu')

@section('title')
	Ubah User #{{ $user->id }}
@endsection

@section('header', 'User')

@section('back', route('user.index'))

@section('value_name', $user->name)

@section('value_email', $user->email)

@section('value_role', $user->role)

@section('value_status', $user->status)

@section('value_noid', $user->identity_number)

@section('value_tel', $user->telephone)

@section('value_branch', $user->branch_id)

@section('value_addr', $user->address)
	
@section('content')
	<form method="POST" action="{{ route('user.update', $user->id) }}">
		@method('PUT')
		@csrf
		@include('user.form')
	</form>
@endsection