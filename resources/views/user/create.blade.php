@extends('layouts.menu')

@section('title', 'Tambah User Baru')

@section('header', 'User')

@section('back', route('user.index'))
	
@section('content')
	<form method="POST" action="{{ route('user.store') }}">
		@csrf
		@include('user.form')
	</form>
@endsection