@extends('layouts.menu-frame')

@section('title', 'Akun Saya')

@section('header', 'Akun Saya')

@section('menu')
	@include('menu-guest')
@endsection

@section('main')
	<div class="card my-3">
		<div class="card-header">
			<span class="navbar-logo">
				<strong>Profil</strong>
			</span>
			<span class="float-right">
				<a href="{{ route('account.profile') }}"> cancel x</a>
			</span>
		</div>
		<div class="card-body">
			<form method="POST" action="{{ route('user.update', $user->id) }}">
				@method('PUT')
				@csrf
				<div class="form-group">
					<label for="edit_name">Nama Lengkap</label>
					<input type="text" class="form-control" id="edit_name" name="name" placeholder="Maksimal 25 karakter" minlength="3" maxlength="25" value="{{ $user->name }}">
				</div>
				<div class="form-group">
					<label for="edit_email">Email</label>
					<input type="email" class="form-control" id="edit_email" name="email" placeholder="Maksimal 30 karakter" maxlength="30" value="{{ $user->email }}">
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="edit_pass">Password</label>
						<input type="password" class="form-control" id="edit_pass" name="password" placeholder="Maksimal 16 karakter" minlength="8" maxlength="16">
					</div>
					<div class="form-group col-md-6">
						<label for="edit_repass">Konfirmasi Password</label>
						<input type="password" class="form-control" id="edit_repass" name="repassword" placeholder="Masukkan kembali password anda" minlength="8" ="" maxlength="16">
					</div>
				</div>
				<div class="form-group">
					<label for="edit_tel">Nomor Telepon</label>
					<input type="text" class="form-control" id="edit_tel" name="telephone" placeholder="Maksimal 15 digit" minlength="12" maxlength="15" value="{{ $user->telephone }}">
				</div>
				<div class="form-group">
					<label for="edit_address">Alamat</label>
					<textarea class="form-control" id="edit_address" name="address" maxlength="100" placeholder="Maksimal 100 karakter">{{ $user->address }}</textarea>
				</div>
				<input type="submit" class="btn btn-success" value="Unggah">
				<input type="reset" class="btn btn-secondary" value="Reset">
			</form>
		</div>
	</div>
@endsection