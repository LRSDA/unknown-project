@extends('layouts.menu-frame')

@section('title', 'Akun Saya')

@section('header', 'Akun Saya')

@section('menu')
	@include('menu-guest')
@endsection

@section('main')
	<div class="card my-3">
		<h5 class="card-header">Profil</h5>
		<div class="card-body row">
			<div class="col-lg-4 col-sm-4">
				<div class="card-title text-muted">Nama Lengkap</div>
			</div>
			<div class="col-lg-8 col-sm-8">
				<p class="card-text">{{ $user->name }}</p>
			</div>
			<div class="col-lg-4 col-sm-4">
				<div class="card-title text-muted">Nomor Telepon</div>
			</div>
			<div class="col-lg-8 col-sm-8">
				@if (empty($user->telephone))
					<p class="card-text text-muted"><em>Belum diisi</em>
				@else
					<p class="card-text">{{ $user->telephone }}
				@endif
				</p>
			</div>
			<div class="col-lg-4 col-sm-4">
				<div class="card-title text-muted">Email</div>
			</div>
			<div class="col-lg-8 col-sm-8">
				<p class="card-text">{{ $user->email }}</p>
			</div>
			<div class="col-lg-4 col-sm-4">
				<div class="card-title text-muted">Alamat</div>
			</div>
			<div class="col-lg-8 col-sm-8">
				@if (empty($user->address))
					<p class="card-text text-muted"><em>Belum diisi</em>
				@else
					<p class="card-text">{{ $user->address }}
				@endif
				</p>
			</div>
		</div>
		<div class="card-footer">
			<a href="{{ route('account.profile.edit') }}" class="card-link">Ubah</a>
		</div>
	</div>
@endsection