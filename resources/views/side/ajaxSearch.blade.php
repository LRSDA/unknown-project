@if (count($roomTypes) != 0)
	@foreach($roomTypes as $roomType)
		<div class="row my-3">
			<div class="col-md-4 col-sm-6 my-auto">
				@if ($roomType->name == 'Superior')
					<img src="{{ URL::asset('img/superior.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $roomType->name }}">
				@elseif ($roomType->name == 'Double Deluxe')
					<img src="{{ URL::asset('img/double_deluxe.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $roomType->name }}">
				@elseif ($roomType->name == 'Executive Deluxe')
					<img src="{{ URL::asset('img/executive_deluxe.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $roomType->name }}">
				@elseif ($roomType->name == 'Junior Suite') 
					<img src="{{ URL::asset('img/junior_suite.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $roomType->name }}">
				@else
					<small class="text-muted mx-auto">Tidak ada gambar tersedia</small>
				@endif
			</div>
			<div class="col-md-8 col-sm-6">
				<span>Kamar {{ $roomType->name }}</span>
				<h2>Rp. {{ number_format($roomType->price_idr, 0, ",", ".") }}</h2>
				<button class="btn btn-primary" onclick="goToDetail('{{ $roomType->name }}')">Detail Kamar</button>
			</div>
		</div>
	@endforeach
@else
	<div class="row my-3">
		<H3>Not Found</H3>
	</div>
@endif