@section('title', 'Pencarian Ketersediaan Kamar')


<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<title>@yield('title') - Grand Atma Hotel</title>
		@include('style')
		<script src="{{ URL::asset('js/searchRoom.js') }}"></script>
    </head>
    <body>
    	@include('navbar')
        <div class="container py-3">
        	<h1>@yield('title')</h1>
        	<form id="searchRoom" >
        		@csrf
        		<div class="row">
        			<div class="col-md-4 my-auto">
        				<div class="form-group row">
							<label for="location" class="col-sm-3 col-form-label col-form-label-sm">Lokasi</label>
							<div class="col-sm-9">
								<select class="form-control form-control-sm" id="location" name="location">
									@foreach($branch as $b)
										<option value="{{ $b->id }}">{{ $b->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="start_date" class="my-auto col-sm-3 col-form-label col-form-label-sm">Tanggal Masuk</label>
							<div class="my-auto col-sm-9">
								<input class="form-control" type="date" id="start_date" name="start_date">
							</div>
						</div>
						<div class="form-group row">
							<label for="end_date" class="my-auto col-sm-3 col-form-label col-form-label-sm">Tanggal Keluar</label>
							<div class="my-auto col-sm-9">
								<input class="form-control" type="date" id="end_date" name="end_date" readonly="true">
							</div>
						</div>
        			</div>
        			<div class="col-md-4">
        				<div class="form-group row">
							<label for="duration" class="col-sm-3 col-form-label col-form-label-sm">Durasi</label>
							<div class="col-sm-6">
								<input class="form-control form-control-sm" id="duration" min=1 onchange="changeCheckOutDate($(this).val())" type="number" name="duration" value="1" required>
							</div>
							<div class="col-sm-3 my-auto">
								<p class="my-auto">Malam</p>
							</div>
						</div>
						<div class="form-group row">
							<label for="room_count" class="col-sm-3 col-form-label col-form-label-sm">Jumlah Kamar</label>
							<div class="col-sm-9">
								<input class="form-control form-control-sm" id="room_count" min=1 type="number" name="room_count" value="1" required>
							</div>
						</div>
        			</div>
        			<div class="col-md-4">
        				<div class="form-group row">
							<label for="guest_adult" class="col-sm-3 col-form-label col-form-label-sm">Dewasa</label>
							<div class="col-sm-9">
								<input class="form-control form-control-sm" id="guest_adult" min=1 type="number" name="guest_adult" value="1" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="guest_child" class="col-sm-3 col-form-label col-form-label-sm">Anak</label>
							<div class="col-sm-9">
								<input class="form-control form-control-sm" id="guest_child" min=0 type="number" name="guest_child" value="0">
							</div>
						</div>
						<input type="submit" class="btn btn-success btn-block" value="Cari Kamar">
        			</div>
        		</div>
        	</form>
        	<div id="listRoom" class="bg-light border rounded p-3">
        		@include('side.ajaxSearch',['roomTypes' => $roomTypes])
        	</div>
        </div>
		{{-- @include('footer') --}}
	</body>
</html>