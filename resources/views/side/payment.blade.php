@section('title', 'Metode Pembayaran')

<!DOCTYPE html>
<html>
<head>
	<title>@yield('title') - Grand Atma Hotel</title>
	@include('style')
</head>
<body>
	@include('navbar')
	<div class="container py-3">
		<h1>@yield('title')</h1>
		<form method="POST" action="{{ route('side.payment.store') }}">
			@csrf
			<input type="hidden" name="location" value="{{ $datas['location'] }}">
			<input type="hidden" name="guestAdult" value="{{ $datas['guestAdult'] }}">
			<input type="hidden" name="guestChild" value="{{ $datas['guestChild'] }}">
			<input type="hidden" name="bed" value="{{ $datas['bed'] }}">
			<input type="hidden" name="id_number" value="{{ $datas['id_number'] }}">
			@if(!empty($datas['name']))
				<input type="hidden" name="name" value="{{ $datas['name'] }}">
			@endif
			@if(!empty($datas['email']))
				<input type="hidden" name="email" value="{{ $datas['email'] }}">
			@endif
			@if(!empty($datas['address']))
				<input type="hidden" name="address" value="{{ $datas['address'] }}">
			@endif
			@if(!empty($datas['telephone']))
				<input type="hidden" name="telephone" value="{{ $datas['telephone'] }}">
			@endif
			<input type="hidden" name="roomCount" value="{{ $datas['roomCount'] }}">
			<input type="hidden" name="roomType" value="{{ $roomType->id }}">
			<input type="hidden" name="code" value="{{ $code }}">
			<input type="hidden" name="grossCost" value="{{ $datas['total_idr'] }}">
			<input type="hidden" name="checkIn" value="{{ $datas['checkIn'] }}">
			<input type="hidden" name="checkOut" value="{{ $datas['checkOut'] }}">
			<input type="hidden" name="deposit" value="{{ $datas['dp_idr'] }}">
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link active" data-toggle="tab" href="#transferPane">Transfer</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#creditPane">Kartu Kredit</a>
				</li>
			</ul>
			<div class="tab-content border rounded-bottom">
				<div id="transferPane" class="tab-pane active p-3">
					<div class="row">
						<div class="col-md-6">
							<div class="card mb-3">
								<div class="card-header">Transfer</div>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-12">Bank Diamond</div>
									</div>
									<div class="row">
										<div class="col-sm-6">Nomor Rekening</div>
										<div class="col-sm-6">770011770022</div>
									</div>
									<div class="row">
										<div class="col-sm-6">Nama Rekening</div>
										<div class="col-sm-6">Grand Atma Hotels</div>
									</div>
									<div class="row">
										<div class="col-sm-6">Jumlah Transfer</div>
										<div class="col-sm-6">Rp. {{ number_format($datas['dp_idr'], 0, ",", ".") }}</div>
									</div>
								</div>
							</div>
							<div class="card mb-3">
								<div class="card-header">Nomor Pesanan</div>
								<div class="card-body">{{ $code }}</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card mb-3">
								<div class="card-header">Rincian Pesanan</div>
								<div class="card-body">
									@if ($datas['location'] == '1')
										<span>Grand Atma Hotels Yogyakarta</span>
									@elseif ($datas['location'] == '2')
										<span>Grand Atma Hotels Bandung</span>
									@endif
									<ul class="my-3">
										<li>{{ date_format(date_create($datas['checkIn']), "d M Y") }}</li>
										<li>{{ $datas['duration'] }} malam</li>
										<li>Jenis Kamar {{ $datas['type'] }}</li>
										<li>{{ $datas['roomCount'] }} kamar</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 text-muted my-auto">Anda sudah membayar?</div>
						<div class="col-sm-6"><button type="submit" class="btn btn-success float-right">Saya Sudah Bayar</button></div>
					</div>
				</div>
				<div id="creditPane" class="tab-pane p-3">
					<div class="row">
						<div class="col-md-6">
							<div class="card mb-3">
								<div class="card-header">Kartu Kredit</div>
								<div class="card-body">
										<div class="form-group">
											<label for="card_number">Nomor Kartu</label>
											<input type="string" class="form-control" id="card_number" placeholder="Masukkan 16 digit pada kartu kredit anda" maxlength="16">
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="valid_date">Berlaku Hingga</label>
													<input type="date" class="form-control" id="valid_date">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="cvv">CVV</label>
													<input type="text" class="form-control" id="cvv" maxlength="3">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label for="card_name">Nama Pemegang Kartu</label>
											<input type="text" class="form-control" id="card_name" maxlength="25">
										</div>
								</div>
							</div>
							<div class="card mb-3">
								<div class="card-header">Rincian Harga</div>
								<div class="card-body">
									<div class="row">
										<div class="col-sm-8">Grand Atma Hotel Kamar {{ $roomType->name }}</div>
										<div class="col-sm-4">Rp. {{ number_format($roomType->price_idr, 0, ",", ".") }}</div>
									</div>
									<div class="row">
										<div class="col-sm-8">Lama Inap</div>
										<div class="col-sm-4">{{ $datas['duration'] }} hari</div>
									</div>
									<div class="row">
										<div class="col-sm-8">Jumlah Kamar</div>
										<div class="col-sm-4">{{ $datas['roomCount'] }} kamar</div>
									</div>
									<div class="row">
										<div class="col-sm-8">Uang Jaminan Reservasi</div>
										<div class="col-sm-4">Rp. {{ number_format($datas['dp_idr'], 0, ",", ".") }}</div>
									</div>
									<br>
									<div class="row">
										<div class="col-sm-8">Total</div>
										<div class="col-sm-4">Rp. {{ number_format($datas['total_idr'], 0, ",", ".") }}</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card mb-3">
								<div class="card-header">Nomor Pesanan</div>
								<div class="card-body">{{ $code }}</div>
							</div>
							<div class="card mb-3">
								<div class="card-header">Rincian Pesanan</div>
								<div class="card-body">
									<span>Grand Atma Hotels Yogyakarta</span>
									<ul class="my-3">
										<li>{{ date_format(date_create($datas['checkIn']), "d M Y") }}</li>
										<li>{{ $datas['duration'] }} malam</li>
										<li>Jenis Kamar {{ $datas['type'] }}</li>
										<li>{{ $datas['roomCount'] }} kamar</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<button type="submit" class="btn btn-success float-right">Saya Sudah Bayar</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>