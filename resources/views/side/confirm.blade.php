@section('title', 'Konfirmasi Pemesanan')

<!DOCTYPE html>
<html>
<head>
	<title>@yield('title') - Grand Atma Hotel</title>
	@include('style')
</head>
<body>
	@include('navbar')
	<div class="container py-3">
		@if($errors->any())
			<div class="alert alert-danger" role="alert">
				Terjadi kesalahan
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<form method="POST" action="{{ route('side.payment') }}">
			@csrf
			<input type="hidden" name="location" value="{{ $datas['location'] }}">
			<input type="hidden" name="guestAdult" value="{{ $datas['guestAdult'] }}">
			<input type="hidden" name="guestChild" value="{{ $datas['guestChild'] }}">
			<input type="hidden" name="bed" value="{{ $datas['bed'] }}">
			@if(!empty($datas['email']))
				<input type="hidden" name="email" value="{{ $datas['email'] }}">
			@endif
			@if(!empty($datas['address']))
				<input type="hidden" name="address" value="{{ $datas['address'] }}">
			@endif
			<input type="hidden" name="id_number" value="{{ $datas['id_number'] }}">
			@if(!empty($datas['telephone']))
				<input type="hidden" name="telephone" value="{{ $datas['telephone'] }}">
			@endif
			<div class="row">
				<div class="col-md-6">
					<div class="card mb-3">
						<div class="card-header">Data Pemesanan</div>
						<div class="card-body">
							<div class="row mb-3">
								<div class="col-sm-4">
									@if ($datas['roomType'] == 'Superior')
										<img src="{{ URL::asset('img/superior.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $datas['roomType'] }}">
									@elseif ($datas['roomType'] == 'Double Deluxe')
										<img src="{{ URL::asset('img/double_deluxe.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $datas['roomType'] }}">
									@elseif ($datas['roomType'] == 'Executive Deluxe')
										<img src="{{ URL::asset('img/executive_deluxe.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $datas['roomType'] }}">
									@elseif ($datas['roomType'] == 'Junior Suite') 
										<img src="{{ URL::asset('img/junior_suite.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $datas['roomType'] }}">
									@else
										<small class="text-muted mx-auto">Tidak ada gambar tersedia</small>
									@endif
								</div>
								<div class="col-sm-8">
									@if($datas['location'] == '1')
										<h5>Grand Atma Hotels Jogja</h5>
									@elseif($datas['location'] == '2')
										<h5>Grand Atma Hotels Bandung</h5>
									@else
										<h5>Grand Atma Hotels</h5>
									@endif
								</div>
							</div>
							<div class="row">
								<label for="name" class="col-sm-4 col-form-label">Nama Pemesan</label>
								<div class="col-sm-8">
									<input type="text" name="name" id="name" readonly class="form-control-plaintext" value="{{ $datas['name'] }}">
								</div>
							</div>
							<div class="row">
								<label for="checkIn" class="col-sm-4 col-form-label">Check In</label>
								<div class="col-sm-8">
									<input type="text" name="checkIn" id="checkIn" readonly class="form-control-plaintext" value="{{ $datas['checkIn'] }}">
								</div>
							</div>
							<div class="row">
								<label for="checkOut" class="col-sm-4 col-form-label">Check Out</label>
								<div class="col-sm-8">
									<input type="text" name="checkOut" id="checkOut" readonly class="form-control-plaintext" value="{{ $datas['checkOut'] }}">
								</div>
							</div>
							<div class="row">
								<label for="duration" class="col-sm-4 col-form-label">Durasi</label>
								<div class="col-sm-1">
									<input type="text" name="duration" id="duration" readonly class="form-control-plaintext" value="{{ $datas['duration'] }}">
								</div>
								<label class="col-sm-7 col-form-label">Malam</label>
							</div>
						</div>
					</div>
					<div class="card mb-3">
						<div class="card-header">Detail Kamar</div>
						<div class="card-body">
							<div class="row">
								<label for="type" class="col-sm-4 col-form-label">Tipe Kamar</label>
								<div class="col-sm-8">
									<input type="text" name="type" id="type" readonly class="form-control-plaintext" value="{{ $datas['roomType'] }}">
								</div>
							</div>
							<div class="row">
								<label for="roomCount" class="col-sm-4 col-form-label">Jumlah Kamar</label>
								<div class="col-sm-8">
									<input type="text" name="roomCount" id="roomCount" readonly class="form-control-plaintext" value="{{ $datas['roomCount'] }}">
								</div>
							</div>
							<div class="row">
								@if(!empty($datas['demand']))
									<label for="demand" class="col-sm-4 col-form-label">Permintaan Khusus</label>
									<div class="col-sm-8">
										<input type="text" name="demand" id="demand" readonly class="form-control-plaintext" value="{{ $datas['demand'] }}">
									</div>
								@else
									<div class="col-sm-4">Permintaan Khusus</div>
									<div class="col-sm-8 text-muted">Tidak ada permintaan khusus</div>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card mb-3">
						<div class="card-header">Rincian Harga</div>
						<div class="card-body">
							<div class="row">
								<label for="price_idr" class="col-sm-6 col-form-label">Grand Atma Hotel Kamar {{ $datas['roomType'] }}</label>
								<label class="col-sm-2 col-form-label">Rp</label>
								<div class="col-sm-4">
									<input type="text" name="price_idr" id="price_idr" readonly class="form-control-plaintext" value="{{ $price_idr->price_idr }}">
								</div>
							</div>
							<div class="row">
								<label for="total_idr" class="col-sm-6 col-form-label">Total</label>
								<label class="col-sm-2 col-form-label">Rp</label>
								<div class="col-sm-4">
									<input type="text" name="total_idr" id="total_idr" readonly class="form-control-plaintext" value="{{ $price_idr->price_idr * $datas['duration'] * $datas['roomCount'] }}">
								</div>
							</div>
							<br>
							<div class="row">
								<label for="dp_idr" class="col-sm-6 col-form-label">Uang Jaminan</label>
								<label class="col-sm-2 col-form-label">Rp</label>
								<div class="col-sm-4">
									<input type="number" name="dp_idr" id="dp_idr" class="form-control" max="{{ $price_idr->price_idr }}" min="50000" step="10000" value="50000">
								</div>
							</div>
						</div>
					</div>
					<div class="my-3 float-right">
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmModal">Lanjut ke Pembayaran</button>
					</div>
				</div>
			</div>
			
			<!-- Confirmation Modal -->
			<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="confirmModalLabel">Konfirmasi Pemesanan</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Tutup">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>Apakah anda setuju dengan pemesanan ini ?</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
							<button type="submit" class="btn btn-success">Setuju</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>