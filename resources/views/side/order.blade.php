@section('title', 'Pemesanan Kamar')

<!DOCTYPE html>
<html>
<head>
	<title>@yield('title') - Grand Atma Hotel</title>
	@include('style')
</head>
<body>
	@include('navbar')
	<div class="container py-3">
		@if($errors->any())
			<div class="alert alert-danger" role="alert">
				Terjadi kesalahan
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<h1>@yield('title')</h1>
		<form method="POST" action="{{ route('side.room.confirm') }}">
			@csrf
			<input type="hidden" name="location" value="{{ $datas['location'] }}">
			<input type="hidden" name="guestAdult" value="{{ $datas['guestAdult'] }}">
			<input type="hidden" name="guestChild" value="{{ $datas['guestChild'] }}">
			<input type="hidden" name="bed" value="{{ $datas['bed'] }}">
			<div class="row">
				<div class="col-md-6 mb-3">
					<div class="card">
						<div class="card-header">
							Data Pribadi
						</div>
						<div class="card-body">
							<div class="form-group row">
								<label for="name" class="col-sm-4 col-form-label col-form-label-sm">Nama Lengkap *</label>
								<div class="col-sm-8 my-auto ">
									<input class="form-control form-control-sm" id="name" type="text" name="name" maxlength="25" value="{{ $user_data['name'] }}">
								</div>
							</div>
							<div class="form-group row">
								<label for="id_number" class="col-sm-4 col-form-label col-form-label-sm">Nomor Identitas *</label>
								<div class="col-sm-8 my-auto">
									<input class="form-control form-control-sm" id="id_number" type="text" name="id_number" maxlength="16" value="{{ $user_data['identity_number'] }}">
								</div>
							</div>
							<div class="form-group row">
								<label for="telephone" class="col-sm-4 col-form-label col-form-label-sm">Nomor Telepon</label>
								<div class="col-sm-8">
									<input class="form-control form-control-sm" id="telephone" type="text" name="telephone" value="{{ $user_data['telephone'] }}">
								</div>
							</div>
							<div class="form-group row">
								<label for="address" class="col-sm-4 col-form-label col-form-label-sm">Alamat</label>
								<div class="col-sm-8">
									<textarea class="form-control" id="address" name="address" maxlength="100" placeholder="Maksimal 100 karakter" >{{ $user_data['address'] }}</textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="email" class="col-sm-4 col-form-label col-form-label-sm">Email</label>
								<div class="col-sm-8">
									<input class="form-control form-control-sm" id="email" type="email" name="email" maxlength="30" value="{{ $user_data['email'] }}">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-4 col-form-label col-form-label-sm">Permintaan Khusus</label>
								<div class="col-sm-8">
									<div class="form-check">
										<input class="form-check-input" name="demand" type="checkbox" value="1 extra bed" id="extra_bed">
										<label class="form-check-label" for="extra_bed">1 Extra Bed</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" name="demand" type="checkbox" value="No smoking" id="no_smoking">
										<label class="form-check-label" for="no_smoking">No Smoking</label>
									</div>
								</div>
							</div>
							<div class="text-muted"><p><em>*) Wajib diisi</em></p></div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							Rincian Pesanan
						</div>
						<div class="card-body">
							<div class="row mb-3">
								<div class="col-md-8">
									@if ($datas['roomType'] == 'Superior')
				    					<img src="{{ URL::asset('img/superior.PNG') }}" class="img-fluid rounded" alt="Kamar Superior">
				    				@elseif ($datas['roomType'] == 'Double Deluxe')
				    					<img src="{{ URL::asset('img/double_deluxe.PNG') }}" class="img-fluid rounded" alt="Kamar Double Deluxe">
				    				@elseif ($datas['roomType'] == 'Executive Deluxe')
				    					<img src="{{ URL::asset('img/executive_deluxe.PNG') }}" class="img-fluid rounded" alt="Kamar Executive Deluxe">
				    				@elseif ($datas['roomType'] == 'Junior Suite')
				    					<img src="{{ URL::asset('img/junior_suite.PNG') }}" class="img-fluid rounded" alt="Kamar Junior Suite">
				    				@else
				    					<small class="text-muted mx-auto">Tidak ada gambar tersedia</small>
				    				@endif
								</div>
							</div>
							<div class="row">
								<label for="duration" class="col-sm-6 col-form-label">Durasi</label>
								<div class="col-sm-1">
									<input type="text" name="duration" readonly class="form-control-plaintext" id="duration" value="{{ $datas['duration'] }}">
								</div>
								<label class="col-sm-5 col-form-label">Malam</label>
							</div>
							<div class="row">
								<label for="checkIn" class="col-sm-6 col-form-label">Check In</label>
								<div class="col-sm-6">
									<input type="text" name="checkIn" readonly class="form-control-plaintext" id="checkIn" value="{{ date_format(date_create($datas['checkIn']), "d M Y") }}">
								</div>
							</div>
							<div class="row">
								<label for="checkOut" class="col-sm-6 col-form-label">Check Out</label>
								<div class="col-sm-6">
									<input type="text" name="checkOut" readonly class="form-control-plaintext" id="checkOut" value="{{ date_format(date_create($datas['checkOut']), "d M Y") }}">
								</div>
							</div>
							<div class="row">
								<label for="roomType" class="col-sm-6 col-form-label">Tipe Kamar</label>
								<div class="col-sm-6">
									<input type="text" name="roomType" readonly class="form-control-plaintext" id="roomType" value="{{ $datas['roomType'] }}">
								</div>
							</div>
							<div class="row">
								<label for="roomCount" class="col-sm-6 col-form-label">Jumlah Kamar</label>
								<div class="col-sm-1">
									<input type="text" name="roomCount" readonly class="form-control-plaintext" id="roomCount" value="{{ $datas['roomCount'] }}">
								</div>
								<label class="col-sm-5 col-form-label">kamar</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row my-3">
				<div class="col-sm-6 col-md-3">
					<a class="btn btn-secondary btn-block" href="{{ route('side.room.search') }}">Ubah Reservasi</a>
				</div>
				<div class="col-sm-6 col-md-3">
					<button type="submit" class="btn btn-primary btn-block">Lanjut ke Konfirmasi</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>