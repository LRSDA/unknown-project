@section('title', $type->name)

@section('description', $type->description)

@section('price', number_format($type->price_idr,0,",","."))

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<title>Kamar @yield('title') - Grand Atma Hotel</title>
		@include('style')
		<script src="{{ URL::asset('js/searchRoom.js') }}"></script>
    </head>
    <body>
    	@include('navbar')
        <div class="container py-3">
        	<h1>@yield('title')</h1>
        	<div class="row">
        		<div class="col-md-4 my-auto">
        			@if ($type->name == 'Superior')
    					<img src="{{ URL::asset('img/superior.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $type->name }}">
    				@elseif ($type->name == 'Double Deluxe')
    					<img src="{{ URL::asset('img/double_deluxe.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $type->name }}">
    				@elseif ($type->name == 'Executive Deluxe')
    					<img src="{{ URL::asset('img/executive_deluxe.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $type->name }}">
    				@elseif ($type->name == 'Junior Suite')
    					<img src="{{ URL::asset('img/junior_suite.PNG') }}" class="img-fluid rounded" alt="Kamar {{ $type->name }}">
    				@else
    					<small class="text-muted mx-auto">Tidak ada gambar tersedia</small>
    				@endif
        		</div>
        		<div class="col-md-8 p-3">
        			<div class="row">
        				<div class="col-md-3 col-sm-4 form-group my-auto">
        					<label for="bed">Tempat Tidur</label>
        					@if(empty($beds))
								<select class="form-control" id="bed" name="bed" disabled="true">
									<option>Tidak tersedia</option>
								</select>
							@else
								<select class="form-control" id="bed" name="bed">
									@foreach($beds as $bed)
										<option value="{{ $bed->quantity }} {{ $bed->name }}">{{ $bed->quantity }} {{ $bed->name }}</option>
									@endforeach
								</select>
							@endif
        				</div>
        				<div class="col-md-6 col-sm-8 my-auto">
        					<h1>Rp. @yield('price')</h1>
        					<p>Harga termasuk pajak</p>
        				</div>
        				<div class="col-md-3 mb-3">
        					@if(empty($beds))
        						<button class="btn btn-secondary btn-block" disabled="true">Pesan Kamar</button>
        					@else
        						<button class="btn btn-success btn-block" onclick="goToOrder('{{ route('side.room.order') }}')">Pesan Kamar</button>
        					@endif
        				</div>
        			</div>
        			<div class="border rounded p-2 ">@yield('description')</div>
        		</div>
        	</div>
        	<div class="my-3 border rounded p-2 ">
        		<h3>Fasilitas</h3>
        		<ul class="list-group">
					@if(empty($facility_description))
						<li class="list-group-item text-muted"><em>Fasilitas tidak tersedia</em></li>
					@else
						@foreach($facility_description as $item)
							<li class="list-group-item">{{ $item->name }} - {{ $item->description }}</li>
						@endforeach
					@endif
				</ul>
        	</div>
            <div class="my-3 border rounded p-2 ">
                <h3>Rincian Kamar</h3>
                <ul class="list-group">
                    @if(empty($facility))
                        <li class="list-group-item text-muted"><em>Fasilitas tidak tersedia</em></li>
                    @else
                        @foreach($facility as $item)
                            <li class="list-group-item">{{ $item->name }}</li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
		{{-- @include('footer') --}}
	</body>
</html>