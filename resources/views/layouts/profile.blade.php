<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<title>{{ config('app.name') }} - @yield('title')</title>
		@include('style')
    </head>
    <body class="bg-dark">
    	@include('navbar')
        <div class="container">
        	<div class="row">
                <div class="col-lg-3">
                    <h1 class="my-3 text-light">Akun Saya</h1>
                    <div class="list-group">
                        <a href="#" class="list-group-item">Profil</a>
                        <a href="#" class="list-group-item">Pemesanan</a>
                        <a href="#" class="list-group-item">History</a>
                    </div>
                </div>
                <div class="col-lg-9 my-2">
                    @if ($success = Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show my-1" role="alert">
                            <h5 class="alert-title">Sukses</h5>
                            <div>{{ $success }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Hilangkan">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show my-1" role="alert">
                            <h5 class="alert-title">Gagal</h5>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Hilangkan">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @yield('content')
                </div>
            </div>
		</div>
		{{-- @include('footer') --}}
	</body>
</html>