@extends('layouts.menu-frame')

@section('menu')
    <a href="{{ route('user.index') }}" class="list-group-item">User</a>
    <a href="{{ route('facility.index') }}" class="list-group-item">Fasilitas</a>
    <a href="{{ route('room-type.index') }}" class="list-group-item">Jenis Kamar</a>
    <a href="{{ route('room.index') }}" class="list-group-item">Kamar</a>
    <a href="{{ route('reservation.index') }}" class="list-group-item">Reservasi</a>
    <a href="{{ route('season.index') }}" class="list-group-item">Season</a>
    <a href="{{ route('price.index') }}" class="list-group-item">Tarif</a>
    <a href="{{ route('report.index') }}" class="list-group-item">Laporan</a>
@endsection

@section('main')
    @if ($success = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show my-1" role="alert">
            <h5 class="alert-title">Sukses</h5>
            <div>{{ $success }}</div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Hilangkan">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show my-1" role="alert">
            <h5 class="alert-title">Gagal</h5>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Hilangkan">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @hasSection('create')
        <a class="btn btn-success my-3" href="@yield('create')"><span class="oi oi-plus" title="oi plus" aria-hidden="true"></span> Tambah @yield('header') Baru</a>
    @endif
    @hasSection('back')
        <a class="btn btn-primary my-3" href="@yield('back')">Kembali</a>
    @endif
    @yield('content')
@endsection