<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<title>@yield('title') - Grand Atma Hotel</title>
		@include('style')
    </head>
    <body class="bg-secondary">
    	@include('navbar')
        <div class="container">
        	<div class="row">
                <div class="col-lg-3">
                    <h1 class="my-3 text-light">@yield('header')</h1>
                    <div class="list-group">
                        @yield('menu')
                    </div>
                </div>
                <div class="col-lg-9 my-2">
                    @yield('main')
                </div>
            </div>
		</div>
		{{-- @include('footer') --}}
	</body>
</html>