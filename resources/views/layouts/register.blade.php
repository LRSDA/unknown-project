<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<title>{{ config('app.name') }} - @yield('title')</title>
		@include('style')
    </head>
    <body background="{{ URL::asset('img/register.jpg') }}">
    	@include('navbar')
        <div class="container">
        	<div class="card @yield('style') mx-auto my-5">
        		<div class="card-header text-center">@yield('card-header')</div>
        		<div class="card-body">@yield('card-body')</div>
        		@hasSection('card-footer')
        		<div class="card-footer">
        			<div class="text-center">
        				@yield('card-footer')
        			</div>
        		</div>
        		@endif
			</div>
		</div>
		{{-- @include('footer') --}}
	</body>
</html>