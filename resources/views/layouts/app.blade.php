<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<title>{{ config('app.name') }} - @yield('title')</title>
		@include('style')
    </head>
    <body>
    	@include('navbar')
        <div class="container">
        	@section('sidebar')
				<h1>This is the master sidebar.</h1>
			@show
			@yield('content')
		</div>
	</body>
</html>