<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Grand Atma Hotel</title>
        @include('style')
    </head>
    <body>
    @include('navbar')

    @yield('card-body')
    
    </body>
</html>
