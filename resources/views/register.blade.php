@extends('layouts.register')

@section('title', 'Registrasi')

@section('style', 'card-register')

@section('card-header', 'REGISTRASI AKUN')

@section('card-body')
	<form method="POST" action="{{ route('guest.create.submit')}}">
		@csrf
		<div class="form-group">
			<label for="username">Nama Pengguna *</label>
			<input class="form-control" id="username" type="text" aria-describedby="username-help" name="username" maxlength="25">
			<small class="form-text text-muted" id="username-help">Maksimal 25 karakter</small>
		</div>
		<div class="form-group">
			<label for="email">Alamat Email *</label>
			<input class="form-control" id="email" type="email" name="email" maxlength="30">
		</div>
		<div class="form-group">
			<label for="idnumber">Nomor Identitas</label>
			<input class="form-control" id="idnumber" type="text" name="identity_number" aria-describedby="idnumber-help" maxlength="16">
			<small class="form-text text-muted" id="idnumber-help">Maksimal 16 digit</small>
		</div>
		<div class="form-group">
			<label for="password">Password *</label>
			<input class="form-control" id="password" type="password" name="password" aria-describedby="password-help" maxlength="15">
			<small class="form-text text-muted" id="password-help">4 - 15 karakter dengan kombinasi angka, huruf dan karakter khusus</small>
		</div>
		<div class="form-group">
			<label for="re-password">Konfirmasi Password *</label>
			<input class="form-control" id="re-password" type="password" name="password_confirmation" aria-describedby="re-password-help" maxlength="15">
			<small class="form-text text-muted" id="re-password-help">Masukkan kembali password anda</small>
		</div>
		<div class="form-group">
			<label for="address">Alamat</label>
			<textarea class="form-control" id="address" type="text" name="address" maxlength="100"></textarea>
			<small class="form-text text-muted" id="address-help">Maksimal 100 huruf</small>
		</div>
		<div class="form-group">
			<label for="phone">Nomor Telepon</label>
			<input class="form-control" id="phone" type="tel" name="telephone" aria-describedby="phone-help" name="phone" maxlength="15">
			<small class="form-text text-muted" id="phone-help">Maksimal 15 digit</small>
		</div>
		<div class="form-group">
			<em class="form-text">*) Wajib diisi</em>
		</div>
		<button type="submit" class="btn btn-primary btn-block">Selesai</button>
	</form>
@endsection

@section('card-footer')
	<a class="d-block small" href="/login">Sudah punya akun?</a>
@endsection