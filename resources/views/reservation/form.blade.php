<div class="p-4 rounded bg-light">
	<h3>@yield('title')</h3>
	<div class="row">
		<div class="form-group col-md-5 col-sm-12">
			<label for="price_room_type">Tamu</label>
			<select class="form-control" id="guest" name="guest">
				@foreach($guests as $guest)
					@if($guest->id == $rsrv->guest_id)
						<option value="{{ $guest->id }}" selected="true">{{ $guest->id }} - {{ $guest->name }}</option>
					@else
						<option value="{{ $guest->id }}">{{ $guest->id }} - {{ $guest->name }}</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6 col-sm-12">
			<label for="check_in_date">Tanggal Check In</label>
			<input class="form-control" id="check_in_date" type="date" name="check_in_date" value="{{ $rsrv->check_in_date }}">
		</div>
		<div class="form-group col-md-6 col-sm-12">
			<label for="check_out_date">Tanggal Check Out</label>
			<input class="form-control" id="check_out_date" type="date" name="check_out_date" value="{{ $rsrv->check_out_date }}">
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6 col-sm-12">
			<label for="number_of_adult">Jumlah Dewasa</label>
			<input class="form-control" id="number_of_adult" type="number" name="number_of_adult" min="1" value="{{ $rsrv->number_of_adult }}">
		</div>
		<div class="form-group col-md-6 col-sm-12">
			<label for="number_of_children">Jumlah Anak - anak</label>
			<input class="form-control" id="number_of_children" type="number" name="number_of_children" min="0" value="{{ $rsrv->number_of_children }}">
		</div>
	</div>
	<input type="submit" class="btn btn-success" value="Unggah">
	<input type="reset" class="btn btn-secondary" value="Reset">
</div>