@extends('layouts.menu')

@section('title', 'Daftar Reservasi')

@section('header', 'Reservasi')

@section('content')
	<div class="mb-3 form-inline">
		<div class="form-group mr-2">
			<input class="form-control searchClue" type="text">
		</div>
		<button class="btn btn-primary" onclick="search(1);"> Search </button>
	</div>
	<table class="table table-sm table-striped table-bordered table-dark text-center">
		<thead class="thead-light">
			<tr>
				<th scope="col" rowspan="2" class="align-middle">Kode</th>
				<th scope="col">Check In</th>
				<th scope="col" rowspan="2" class="align-middle">Nama Tamu</th>
				<th scope="col">Reservasi</th>
				<th scope="col" rowspan="2" class="align-middle">Aksi</th>
			</tr>
			<tr>
				<th scope="col">Check Out</th>
				<th scope="col">Pembayaran</th>
			</tr>
		</thead>
		<tbody class="implementSearch">
			@foreach($reservations as $rsrv)
				<tr>
					<td scope="row" class="align-middle search" name="{{ $rsrv->code }}" rowspan="2">{{ $rsrv->code }}</td>
					<td class="align-middle">{{ date('d M Y', strtotime($rsrv->check_in_date)) }}</td>
					<td class="align-middle search" name="{{ $rsrv->name }}" rowspan="2">{{ $rsrv->name }}</td>
					<td class="align-middle">{{ date('d M Y', strtotime($rsrv->reservation_date)) }}</td>
					<td class="align-middle" rowspan="2">
						<a class="btn btn-info btn-sm my-1" href="{{ route('reservation.edit', $rsrv->id) }}" role="button">Ubah</a>
						<form method="POST" action="{{ route('reservation.destroy', $rsrv->id) }}">
							@method('DELETE')
							@csrf
							<input type="submit" class="btn btn-danger btn-sm my-1" value="Hapus">
						</form>
					</td>
				</tr>
				<tr>
					<td class="align-middle">{{ date('d M Y', strtotime($rsrv->check_out_date)) }}</td>
					<td class="align-middle">{{ date('d M Y', strtotime($rsrv->payment_date)) }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection

