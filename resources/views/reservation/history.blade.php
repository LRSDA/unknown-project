@extends('layouts.menu-frame')

@section('title', 'Akun Saya')

@section('header', 'Akun Saya')

@section('menu')
	@include('menu-guest')
@endsection

@section('main')
	<div class="card my-3">
		<h5 class="card-header">History Pemesanan</h5>
		<div class="card-body">
			@foreach($reservations as $rsrv)
				<div class="card mb-3">
					<div class="card-body">
						@if ($rsrv->branch_id == '1')
							<h5>Grand Atma Hotel Yogyakarta</h5>
						@elseif ($rsrv->branch_id == '2')
							<h5>Grand Atma Hotel Bandung</h5>
						@else
							<h5>Grand Atma Hotel</h5>
						@endif
						<div class="row">
							<div class="col-lg-4 col-sm-4">
								<div class="card-title text-muted">Kode Pemesanan</div>
							</div>
							<div class="col-lg-8 col-sm-8">
								<p class="card-text">{{ $rsrv->code }}</p>
							</div>
						</div>
					</div>
					<div class="card-footer">
						<a href="{{ route('reservation.history.detail', $rsrv->id) }}" class="card-link">Lihat Selengkapnya</a>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endsection