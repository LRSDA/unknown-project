<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<title>{{ $reservation->code }} - Grand Atma Hotel</title>
		@include('style')
        <script>
            $(document).ready(function(){
                window.print();
            })
        </script>
    </head>
    <body class="bg-light m-3 border border-dark">
        <div class="container">
        	<div style="width: 75%" class="mx-auto">
        		<img class="img-fluid" src="{{ URL::asset('img/grand_atma_hotels.png') }}">
        	</div>
        	<div class="my-3">
        		<p class="text-center">Jl. P. Mangkubumi No.18, Yogyakarta 55233<br>Telp. (0274) 487711</p>
        	</div>

        	<p class="border border-dark border-right-0 border-left-0 text-center">
				<strong>TANDA TERIMA PEMESANAN</strong>
        	</p>
        	<div class="row my-3">
        		<div class="col-md-3 col-sm-4">ID Booking</div>
        		<div class="col-md-9 col-sm-8">{{ $reservation->code }}</div>
        		<div class="col-md-3 col-sm-4">Tanggal</div>
        		<div class="col-md-9 col-sm-8">{{ date('d-M-Y', strtotime($reservation->reservation_date)) }}</div>
        		<div class="col-md-3 col-sm-4">Nama</div>
        		<div class="col-md-9 col-sm-8">{{ $guest->name }}</div>
        		<div class="col-md-3 col-sm-4">Alamat</div>
        		<div class="col-md-9 col-sm-8">{{ $guest->address }}</div>
        	</div>

        	<p class="border border-dark border-right-0 border-left-0 text-center">
				<strong>DETAIL PEMESANAN</strong>
        	</p>
        	<div class="row my-3">
        		<div class="col-md-3 col-sm-4">Check In</div>
        		<div class="col-md-9 col-sm-8">{{ date('d-M-Y', strtotime($reservation->check_in_date)) }}</div>
        		<div class="col-md-3 col-sm-4">Check Out</div>
        		<div class="col-md-9 col-sm-8">{{ date('d-M-Y', strtotime($reservation->check_out_date)) }}</div>
        		<div class="col-md-3 col-sm-4">Dewasa</div>
        		<div class="col-md-9 col-sm-8">@empty($reservation->number_of_adult)
        				0
        			@else
        				{{ $reservation->number_of_adult }}
        			@endif</div>
        		<div class="col-md-3 col-sm-4">Anak-anak</div>
        		<div class="col-md-9 col-sm-8">@empty($reservation->number_of_child)
        				0
        			@else
        				{{ $reservation->number_of_child }}
        			@endif</div>
        		<div class="col-md-3 col-sm-4">Tanggal Pembayaran</div>
        		<div class="col-md-9 col-sm-8">{{ date('d-M-Y', strtotime($reservation->payment_date)) }}</div>
        	</div>

        	<p class="border border-dark border-right-0 border-left-0 text-center">
				<strong>DETAIL KAMAR</strong>
        	</p>
        	<div class="my-3">
        		<table class="table table-bordered">
        			<thead>
        				<tr>
        					<th scope="col">Jenis Kamar</th>
        					<th scope="col">Harga</th>
        					<th scope="col">Total</th>
        				</tr>
        			</thead>
        			<tbody>@foreach($rooms as $room)
        				<tr>
        					<th scope="row">{{ $room->name }}</th>
        					<td>Rp. {{ number_format(intval($room->price_idr), 0, ",", ".") }}</td>
        					<td>Rp. {{ number_format(intval($room->price_idr), 0, ",", ".") }}</td>
        				</tr>@endforeach
        				<tr>
							<th scope="row" colspan="2"></th>
							<td><strong>Rp. {{ number_format(intval($total), 0, ",", ".") }}</strong></td>
						</tr>
        			</tbody>
        		</table>
        	</div>
        	<div class="my-3">
        		<span>Permintaan Khusus :</span>
        		<ul>@empty($demands)
        				<li class="text-muted"><em>Tidak ada permintaan khusus</em></li>
        			@else
        				@foreach($demands as $demand)
        					<li>{{ $demand->demand }}</li>
        				@endforeach
        		@endif</ul>
        	</div>
		</div>
		{{-- @include('footer') --}}
	</body>
</html>