@extends('layouts.menu')

@section('title')
	Ubah Reservasi #{{ $rsrv->code }}
@endsection

@section('header', 'Reservasi')

@section('back', route('reservation.index'))
	
@section('content')
	<form method="POST" action="{{ route('reservation.update', $rsrv->id) }}">
		@method('PUT')
		@csrf
		@include('reservation.form')
	</form>
@endsection