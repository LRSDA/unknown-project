@extends('layouts.menu-frame')

@section('title', 'Akun Saya')

@section('header', 'Akun Saya')

@section('menu')
	@include('menu-guest')
@endsection

@section('main')
	<div class="card my-3">
		<h5 class="card-header">Detail Pemesanan</h5>
		<div class="card-body row">
			<div class="col-md-4">
				<div class="row">
					<div class="col-sm-5 col-md-12 text-muted">
						<div class="card-title text-muted">Nama Pemesan</div>
					</div>
					<div class="col-sm-7 col-md-12">
						<p class="card-text">{{ $name }}</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-sm-5 col-md-12">
						<div class="card-title text-muted">Tanggal Reservasi</div>
					</div>
					<div class="col-sm-7 col-md-12">
						<p class="card-text">{{ date('d M Y', strtotime($reservation->reservation_date)) }}</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-sm-5 col-md-12">
						<div class="card-title text-muted">Kode Pemesanan</div>
					</div>
					<div class="col-sm-7 col-md-12">
						<p class="card-text">{{ $reservation->code }}</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="card my-3">
		<h5 class="card-header">Pembayaran</h5>
		<div class="card-body row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-sm-5 col-md-12 text-muted">
						<div class="card-title text-muted">Jenis Pembayaran</div>
					</div>
					<div class="col-sm-7 col-md-12">
						@empty($reservation->number_of_credit_card)
							<p class="card-text">Transfer</p>
						@else
							<p class="card-text">Kartu Kredit</p>
						@endif
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-sm-5 col-md-12 text-muted">
						<div class="card-title text-muted">Status Pembayaran</div>
					</div>
					<div class="col-sm-7 col-md-12">
						@empty($reservation->payment_date)
							<p class="card-text">Belum Lunas</p>
						@else
							<p class="card-text">Lunas</p>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="card my-3">
		<h5 class="card-header">Detail Pemesanan</h5>
		<div class="card-body row">
			<div class="col-lg-4 col-sm-5">
				<div class="card-title text-muted">Tanggal Check In</div>
			</div>
			<div class="col-lg-8 col-sm-7">
				<p class="card-text">{{ date('d M Y', strtotime($reservation->check_in_date)) }}</p>
			</div>
			<div class="col-lg-4 col-sm-5">
				<div class="card-title text-muted">Tanggal Check Out</div>
			</div>
			<div class="col-lg-8 col-sm-7">
				<p class="card-text">{{ date('d M Y', strtotime($reservation->check_out_date)) }}</p>
			</div>
			<div class="col-lg-4 col-sm-5">
				<div class="card-title text-muted">Jumlah Orang Dewasa</div>
			</div>
			<div class="col-lg-8 col-sm-7">
				@empty($reservation->number_of_adult)
					<p class="card-text text-muted">Tidak ada
				@else
					<p class="card-text">{{ $reservation->number_of_adult }}
				@endif</p>
			</div>
			<div class="col-lg-4 col-sm-5">
				<div class="card-title text-muted">Jumlah Anak-anak</div>
			</div>
			<div class="col-lg-8 col-sm-7">
				@empty($reservation->number_of_child)
					<p class="card-text text-muted">Tidak ada
				@else
					<p class="card-text">{{ $reservation->number_of_child }}
				@endif</p>
			</div>
			<div class="col-lg-4 col-sm-5">
				<div class="card-title text-muted">Tanggal Pembayaran</div>
			</div>
			<div class="col-lg-8 col-sm-7">
				<p class="card-text">{{ date('d M Y', strtotime($reservation->payment_date)) }}</p>
			</div>

			<div class="col-lg-12">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th scope="col">Tipe Kamar</th>
							<th scope="col">Harga</th>
							<th scope="col">Lama Inap</th>
							<th scope="col">Total</th>
						</tr>
					</thead>
					<tbody>
						@foreach($rooms as $room)
							<tr>
								<th scope="row">{{ $room->name }}</th>
								<td>Rp. {{ number_format(intval($room->price_idr), 0, ",", ".") }}</td>
								<td>{{ $days }} hari</td>
								<td>Rp. {{ number_format(intval($room->price_idr) * $days, 0, ",", ".") }}</td>
							</tr>
						@endforeach
						<tr>
							<th scope="row" colspan="3"></th>
							<td>Rp. {{ number_format(intval($total), 0, ",", ".") }}</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="col-lg-4 col-sm-5">
					<div class="card-title text-muted">Permintaan Khusus</div>
				</div>
				<div class="col-lg-8 col-sm-7">
					@empty($demands)
						<p class="card-text text-muted">Tidak ada</p>
					@else
						<ul>
							@foreach($demands as $demand)
								<li class="card-text">{{ $demand->demand }}</li>
							@endforeach
						</ul>
					@endif
				</div>
		</div>
		<div class="card-footer">
			<a href="{{ route('reservation.history') }}" class="card-link">Kembali</a>
		</div>
	</div>
@endsection