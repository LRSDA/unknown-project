@extends('layouts.auth-form')

@section('title', 'Login')

@section('style', 'card-login')

@section('card-header', 'LOGIN')

@section('card-body')
	<form method="POST" action="{{ route('login.submit') }}">
		@csrf
		<div class="form-group">
			<label for="loginEmail">Alamat Email</label>
			<input class="form-control" id="loginEmail" name='email' type="email" placeholder="Isi dengan email anda" value="{{ old('email') }}">
		</div>
		<div class="form-group">
			<label for="loginPassword">Password</label>
			<input class="form-control" id="loginPassword" name='password' type="password" placeholder="Isi dengan password anda">
		</div>
		<button type="submit" class="btn btn-primary btn-block">Masuk</button>
	</form>
@endsection

@section('card-footer')
	<a class="d-block small" href="{{ route('forgot') }}">Lupa password?</a>
	<a class="d-block small" href="{{ route('register') }}">Belum punya akun?</a>
@endsection