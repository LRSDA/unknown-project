<table class="table table-sm table-striped table-bordered text-center">
	<thead>
		<th scope="col">No</th>
		<th scope="col">Jenis Kamar</th>
		<th scope="col">Personal</th>
		<th scope="col">Group</th>
		<th scope="col">Total</th>
	</thead>
	@if(empty($datas))
		<tr>
			<th scope="row" colspan="5">
				Tidak ada data
			</th>
		</tr>
	@else
		@foreach($datas as $key => $data)
			<tr>
				<th scope="row">{{ $data['num'] }}</th>
				<td>{{ $key }}</td>
				<td>{{ $data['Personal'] }}</td>
				<td>{{ $data['Grup'] }}</td>
				<td>{{ $data['total'] }}</td>
			</tr>
		@endforeach
	@endif
</table>