<table class="table table-sm table-striped table-bordered text-center">
	<thead>
		<th scope="col">No</th>
		<th scope="col">Bulan</th>
		<th scope="col">Jumlah Pelanggan Baru</th>
	</thead>
	@if(empty($datas))
		<tr>
			<th scope="row" colspan="3">
				Tidak ada data
			</th>
		</tr>
	@else
		@foreach($datas as $key => $data)
			<tr>
				<th scope="row">{{ $key }}</th>
				<td>{{ $data->month }}</td>
				<td>{{ $data->count_guest }}</td>
			</tr>
		@endforeach
	@endif
</table>