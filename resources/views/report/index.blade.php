@extends('layouts.menu')

@section('title', 'Laporan')

@section('header', 'Laporan')

@section('content')
<script src="{{ URL::asset('js/report.js') }}"></script>
<div class="p-4 rounded bg-light">
	<h3>@yield('title')</h3>
	<form class="pr-3">
		@csrf
		
		<div class="form-group row">
			<label for="report_type" class="col-lg-3 col-form-label text-muted">Jenis Laporan</label>
			<select id="report_type" name="report_type" class="col-lg-9 form-control my-auto">
				<option value="1">5 Pelanggan dengan Reservasi Terbanyak</option>
				<option value="2">Jumlah Pelanggan Baru</option>
				<option value="3">Pendapatan Pertahun</option>
				<option value="4">Pendapatan Perbulan per Jenis Tamu</option>
				<option value="5">Jumlah Tamu per Jenis Kamar</option>
			</select>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group row">
					<label for="year" class="col-sm-6 col-form-label text-muted">Tahun</label>
					<select id="year" name="year" class="col-sm-6 form-control my-auto">

					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group row hidden">
					<label for="month" class="col-sm-4 col-form-label text-muted">Bulan</label>
					<select id="month" name="month"  class="col-sm-8 form-control">
						<option value = "01" Selected>Januari</option>
						<option value = "02">Febuari</option>
						<option value = "03">Maret</option>
						<option value = "04">April</option>
						<option value = "05">Mei</option>
						<option value = "06">Juni</option>
						<option value = "07">Juli</option>
						<option value = "08">Agustus</option>
						<option value = "09">September</option>
						<option value = "10">Oktober</option>
						<option value = "11">November</option>
						<option value = "12">Desember</option>
					</select>
				</div>
			</div>
		</div>
	</form>
	<div id="tabel_report">
		
	</div>
</div>
@endsection

