<table class="table table-sm table-striped table-bordered text-center">
	<thead>
		<th scope="col">No</th>
		<th scope="col">Tahun</th>
		<th scope="col">Jumlah</th>
	</thead>
	@if(empty($datas))
		<tr>
			<th scope="row" colspan="3">
				Tidak ada data
			</th>
		</tr>
	@else
		@foreach($datas as $key => $data)
			<tr>
				<th scope="row">{{ $key }}</th>
				<td>{{ $data->year }}</td>
				<td>Rp. {{ number_format(intval($data->total), 0, ",", ".") }}</td>
			</tr>
		@endforeach
	@endif
</table>