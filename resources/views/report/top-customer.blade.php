<table class="table table-sm table-striped table-bordered text-center">
	<thead>
		<th scope="col">No</th>
		<th scope="col">Nama Customer</th>
		<th scope="col">Jumlah Reservasi</th>
		<th scope="col">Total Pembayaran</th>
	</thead>
	@if(empty($datas))
		<tr>
			<th scope="row" colspan="4">
				Tidak ada data
			</th>
		</tr>
	@else
		@foreach($datas as $key => $data)
			<tr>
				<th scope="row">{{ $key }}</th>
				<td>{{ $data->name }}</td>
				<td>{{ $data->count_reservation }}</td>
				<td>Rp. {{ number_format(intval($data->cost_reservation), 0, ",", ".") }}</td>
			</tr>
		@endforeach
	@endif
</table>