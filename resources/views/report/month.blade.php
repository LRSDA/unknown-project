<table class="table table-sm table-striped table-bordered text-center">
	<thead>
		<th scope="col">No</th>
		<th scope="col">Bulan</th>
		<th scope="col">Personal</th>
		<th scope="col">Grup</th>
		<th scope="col">Total</th>
	</thead>
	@if(empty($datas))
		<tr>
			<th scope="row" colspan="5">
				Tidak ada data
			</th>
		</tr>
	@else
		@php 
			$count = 1;
		@endphp
		@foreach($datas as $key => $data)
			<tr>
				<th scope="row">{{ $count++ }}</th>
				<td>{{ $data['Month'] }}</td>
				<td>Rp. {{ number_format(intval($data['Personal']), 0, ",", ".") }}</td>
				<td>Rp. {{ number_format(intval($data['Grup']), 0, ",", ".") }}</td>
				<td>Rp. {{ number_format(intval($data['total']), 0, ",", ".") }}</td>
			</tr>
		@endforeach
	@endif
</table>