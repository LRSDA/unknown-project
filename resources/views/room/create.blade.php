@extends('layouts.menu')

@section('title', 'Tambah Kamar Baru')

@section('header', 'Kamar')

@section('back', route('room.index'))
	
@section('content')
	<form method="POST" action="{{ route('room.store') }}">
		@csrf
		@include('room.form')
	</form>
@endsection