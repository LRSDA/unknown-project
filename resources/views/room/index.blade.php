@extends('layouts.menu')

@section('title', 'Daftar Kamar')

@section('header', 'Kamar')

@section('create', route('room.create'))

@section('content')
	<table class="table table-sm table-striped table-bordered table-dark text-center">
		<thead class="thead-light">
			<tr>
				<th scope="col">Kode</th>
				<th scope="col" class="align-middle">Posisi</th>
				<th scope="col">Status</th>
				<th scope="col" class="align-middle">Deskripsi</th>
				<th scope="col">Harga</th>
				<th scope="col">Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($rooms as $room)
				<tr>
					<th scope="row" rowspan="2" class="align-middle">{{ $room->code }}</th>
					<td class="align-middle">Lt. {{ $room->floor }}</td>
					@if(empty($room->status))
						<td class="align-middle" rowspan="2">-</td>
					@else
						<td class="align-middle" rowspan="2">{{ $room->status }}</td>
					@endif
					@if(empty($room->name))
						<td class="align-middle" rowspan="2">-</td>
					@else
						<td class="align-middle">{{ $room->name }}</td>
					@endif
					<td class="align-middle" rowspan="2">Rp. {{ number_format($room->price_idr,0,",",".") }}</td>
					<td rowspan="2">
						<a class="btn btn-info btn-sm my-1" href="{{ route('room.edit', $room->id) }}" role="button">Ubah</a>
						<form method="POST" action="{{ route('room.destroy', $room->id) }}">
							@method('DELETE')
							@csrf
							<input type="submit" class="btn btn-danger btn-sm my-1" value="Hapus">
						</form>
					</td>
				</tr>
				<tr>
					<td class="align-middle">No. {{ $room->number }}</td>
					<td class="align-middle">{{ $room->description }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection

