@extends('layouts.menu')

@section('title')
	Ubah Kamar {{ $room->code }}
@endsection

@section('header', 'Kamar')

@section('back', route('room.index'))

@section('value_number', $room->number)

@section('value_floor', $room->floor)

@section('value_room_type', true)
	
@section('content')
	<form method="POST" action="{{ route('room.update', $room->id) }}">
		@method('PUT')
		@csrf
		@include('room.form')
	</form>
@endsection