<div class="p-4 rounded bg-light">
	<h3>@yield('title')</h3>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="room_number">Nomor Kamar</label>
			<input class="form-control" id="room_number" type="number" name="room_number" min="1" max="25" placeholder="1 - 25" 
				@hasSection('value_number')
					value=@yield('value_number')
				@endif
			>
		</div>
		<div class="form-group col-lg-6">
			<label for="room_floor">Lantai Kamar</label>
			<input class="form-control" id="room_floor" type="number" name="room_floor" min="1" max="5" placeholder="1 - 5"
				@hasSection('value_floor')
					value=@yield('value_floor')
				@endif
			>
		</div>
	</div>
	<div class="form-group">
		<label for="room_type">Tipe Kamar</label>
		<select class="form-control" id="room_type" name="room_type">
			@foreach($types as $type)
				@hasSection('value_room_type')
					@if ($type->id == $room->room_type_id)
						<option value="{{ $type->id }}" selected="true">{{ $type->name }} - {{ $type->code }} (Rp. {{ $type->price_idr }}, {{ $type->description }})</option>
					@else
						<option value="{{ $type->id }}">{{ $type->name }} - {{ $type->code }} (Rp. {{ $type->price_idr }}, {{ $type->description }})</option>
					@endif
				@else
					<option value="{{ $type->id }}">{{ $type->name }} - {{ $type->code }} (Rp. {{ $type->price_idr }}, {{ $type->description }})</option>
				@endif
			@endforeach
		</select>
	</div>
	<input type="submit" class="btn btn-success" value="Unggah">
	<input type="reset" class="btn btn-secondary" value="Reset">
</div>