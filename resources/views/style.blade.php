<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/open-iconic-bootstrap.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/homepage.css') }}">
<script src="{{ URL::asset('js/custom.js') }}"></script>
<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script src="{{ URL::asset('js/popper.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.js') }}"></script>