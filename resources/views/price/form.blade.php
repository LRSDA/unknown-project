<div class="p-4 rounded bg-light">
	<h3>@yield('title')</h3>
	<div class="row">
		<div class="form-group col-md-5 col-sm-6">
			<label for="price_price">Harga (Rp.)</label>
			<input class="form-control" id="price_price" type="number" name="price_idr" min="1000" step="1000" 
				@hasSection('value_price')
					value="@yield('value_price')"
				@endif
			>
		</div>
		<div class="form-group col-md-7 col-sm-6">
			<label for="price_status">Status</label>
			<!-- <input class="form-control" id="price_status" type="text" name="status"
				@hasSection('value_status')
					value="@yield('value_status')"
				@endif
			> -->
			<select class="form-control" id="price_status" type="text" name="status" @hasSection('value_status')
					value="@yield('value_status')"
				@endif>
				
				<option value="Inactive" @hasSection('value_status')
						@if ( $price->status == "Inactive" ) selected @endIf
					@endIf>
					Inactive
				</option>
				<option value="Active"  @hasSection('value_status')
						@if ( $price->status == "Active" ) selected @endIf
					@endIf>
					Active
				</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-5 col-sm-12">
			<label for="price_room_type">Tipe Kamar</label>
			<select class="form-control" id="price_room_type" name="room_type_id">
				@foreach($roomTypes as $roomType)
					@hasSection('value_room_type')
						@if($price->room_type_id == $roomType->id)
							<option value="{{ $roomType->id }}" selected="true">{{ $roomType->name }} ({{ $roomType->code }})</option>
						@else
							<option value="{{ $roomType->id }}">{{ $roomType->name }} ({{ $roomType->code }})</option>
						@endif
					@else
						<option value="{{ $roomType->id }}">{{ $roomType->name }} ({{ $roomType->code }})</option>
					@endif
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-7 col-sm-12">
			<label for="price_season">Season</label>
			<select class="form-control" id="price_season" name="season_id">
				@foreach($seasons as $season)
					@hasSection('value_season')
						@if($price->season_id == $season->id)
							<option value="{{ $season->id }}" selected="true">{{ $season->description }} ({{ $season->type }})</option>
						@else
							<option value="{{ $season->id }}">{{ $season->description }} ({{ $season->type }})</option>
						@endif
					@else
						<option value="{{ $season->id }}">{{ $season->description }} ({{ $season->type }})</option>
					@endif
				@endforeach
			</select>
		</div>
	</div>
	<input type="submit" class="btn btn-success" value="Unggah">
	<input type="reset" class="btn btn-secondary" value="Reset">
</div>