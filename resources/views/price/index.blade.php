@extends('layouts.menu')

@section('title', 'Daftar Tarif')

@section('header', 'Tarif')

@section('create', route('price.create'))

@section('content')
	<table class="table table-sm table-striped table-bordered table-dark text-center">
		<thead class="thead-light">
			<tr>
				<th scope="col">#</th>
				<th scope="col">Harga</th>
				<th scope="col">Status</th>
				<th scope="col">Tipe Kamar</th>
				<th scope="col">Season</th>
				<th scope="col">Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($prices as $price)
				<tr>
					<th scope="row" class="align-middle" rowspan="2">{{ $price->id }}</th>
					<td class="align-middle" rowspan="2">Rp. {{ number_format($price->price_idr,0,",",".") }}</td>
					<td class="align-middle" rowspan="2">{{ $price->status }}</td>
					<td class="align-middle">{{ $price->code }}</td>
					<td class="align-middle">{{ $price->description }}</td>
					<td class="align-middle" rowspan="2">
						<a class="btn btn-info btn-sm my-1" href="{{ route('price.edit', $price->id) }}" role="button">Ubah</a>
						<form method="POST" action="{{ route('price.destroy', $price->id) }}">
							@method('DELETE')
							@csrf
							<input type="submit" class="btn btn-danger btn-sm my-1" value="Hapus">
						</form>
					</td>
				</tr>
				<tr>
					<td class="align-middle">{{ $price->name }}</td>
					<td class="align-middle">{{ $price->type }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection

