@extends('layouts.menu')

@section('title', 'Tambah Tarif Baru')

@section('header', 'Tarif')

@section('back', route('price.index'))
	
@section('content')
	<form method="POST" action="{{ route('price.store') }}">
		@csrf
		@include('price.form')
	</form>
@endsection