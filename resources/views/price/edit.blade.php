@extends('layouts.menu')

@section('title')
	Ubah Tarif #{{ $price->id }}
@endsection

@section('header', 'Tarif')

@section('back', route('price.index'))

@section('value_price', $price->price_idr)

@section('value_status', $price->status)

@section('value_room_type', $price->room_type_id)

@section('value_season', $price->season_id)
	
@section('content')
	<form method="POST" action="{{ route('price.update', $price->id) }}">
		@method('PUT')
		@csrf
		@include('price.form')
	</form>
@endsection