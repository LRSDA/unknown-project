@extends('layouts.auth-form')

@section('title', 'Reset Password')

@section('style', 'card-register')

@section('card-header', 'RESET PASSWORD')

@section('card-body')
	<form method="POST" action="{{ route('reset.submit')}}">
		@csrf
		
		<div class="form-group">
			<input class="form-control" id="username" type="text" aria-describedby="username-help" name="username" maxlength="10" value="{{ $username }}" hidden>
		</div>
		<div class="form-group">
			<input class="form-control" id="email" type="email" name="email" maxlength="30" value="{{ $email }}" hidden>
		</div>
		<div class="form-group">
			<input class="form-control" id="idnumber" type="text" name="identity_number" aria-describedby="idnumber-help" value="{{ $identity_number }}" maxlength="16" hidden>
		</div>
		<div class="form-group">
			<label for="password">Password *</label>
			<input class="form-control" id="password" type="password" name="password" aria-describedby="password-help" maxlength="15">
			<small class="form-text text-muted" id="password-help">4 - 15 karakter dengan kombinasi angka, huruf dan karakter khusus</small>
		</div>
		<div class="form-group">
			<label for="re-password">Konfirmasi Password *</label>
			<input class="form-control" id="re-password" type="password" name="password_confirmation" aria-describedby="re-password-help" maxlength="15">
			<small class="form-text text-muted" id="re-password-help">Masukkan kembali password anda</small>
		</div>
		<div class="form-group">
			<input class="form-control" id="phone" type="tel" name="telephone" aria-describedby="phone-help" name="phone" maxlength="15" value="{{ $telephone }}" hidden>
		</div>
		<button type="submit" class="btn btn-primary btn-block">Selesai</button>
	</form>
@endsection

@section('card-footer')
	<a class="d-block small" href="/login">Sudah punya akun?</a>
@endsection