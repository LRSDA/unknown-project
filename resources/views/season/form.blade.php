<div class="p-4 rounded bg-light">
	<h3>@yield('title')</h3>
	<div class="row">
		<div class="form-group col-sm-6">
			<label for="season_type">Tipe Promo</label>
			<select class="form-control" id="season_type" name="type">
				@foreach($types as $type)
					@hasSection('value_type')
						@if($season->type == $type)
							<option value="{{ $type }}" selected="true">{{ $type }}</option>
						@else
							<option value="{{ $type }}">{{ $type }}</option>
						@endif
					@else
						<option value="{{ $type }}">{{ $type }}</option>
					@endif
				@endforeach
			</select>
		</div>
		<div class="form-group col-sm-6">
			<label for="season_status">Status Promo</label>
			<!-- <input class="form-control" id="season_status" type="text" name="status"
				@hasSection('value_status')
					value="@yield('value_status')"
				@endif
			> -->
			<select class="form-control" id="season_status" type="text" name="status" @hasSection('value_status')
					value="@yield('value_status')"
				@endif>
				
				<option value="Inactive"  @hasSection('value_status')
						@if ( $season->status == "Inactive" ) selected @endIf
					@endIf >
					Inactive
				</option>
				<option value="Active"  @hasSection('value_status')
						@if ( $season->status == "Active" ) selected @endIf
					@endIf >
					Active
				</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-4">
			<label for="season_start">Tanggal Mulai</label>
			<input class="form-control" type="date" id="season_start" name="start_date"
				@hasSection('value_start')
					value=@yield('value_start') 
				@else
					value=@yield('today_date')
				@endif
			>
		</div>
		<div class="form-group col-md-4">
			<label for="season_end">Tanggal Berakhir</label>
			<input class="form-control" type="date" id="season_end" name="end_date"
				@hasSection('value_end')
					value=@yield('value_end') 
				@endif
			>
		</div>
		<div class="form-group col-md-4">
			<label for="season_create">Tanggal Buat</label>
			<input class="form-control" type="date" id="season_create" name="create_date" max=@yield('today_date') 
				@hasSection('value_create')
					value=@yield('value_create') 
				@else
					value=@yield('today_date')
				@endif
			>
		</div>
	</div>
	<div class="form-group">
		<label for="season_desc">Deskripsi Promo</label>
		<textarea class="form-control" id="season_desc" name="description" maxlength="100" placeholder="Maksimal 100 karakter">@hasSection('value_desc')@yield('value_desc')@endif</textarea>
	</div>
	<input type="submit" class="btn btn-success" value="Unggah">
	<input type="reset" class="btn btn-secondary" value="Reset">
</div>