@extends('layouts.menu')

@section('title', 'Daftar Season')

@section('header', 'Season')

@section('create', route('season.create'))

@section('content')
	<table class="table table-sm table-striped table-bordered table-dark text-center">
		<thead class="thead-light">
			<tr>
				<th scope="col">ID #</th>
				<th scope="col">Tipe</th>
				<th scope="col">Tgl. Mulai</th>
				<th scope="col">Tgl. Berakhir</th>
				<th scope="col">Tgl. Buat</th>
				<th scope="col">Status</th>
				<th scope="col">Deskripsi</th>
				<th scope="col">Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($seasons as $season)
				<tr>
					<th scope="row" class="align-middle">{{ $season->id }}</th>
					<td class="align-middle">{{ $season->type }}</td>
					<td class="align-middle">{{ $season->start_date }}</td>
					<td class="align-middle">{{ $season->end_date }}</td>
					<td class="align-middle">{{ $season->create_date }}</td>
					<td class="align-middle">{{ $season->status }}</td>
					<td class="align-middle">{{ $season->description }}</td>
					<td>
						<a class="btn btn-info btn-sm my-1" href="{{ route('season.edit', $season->id) }}" role="button">Ubah</a>
						<form method="POST" action="{{ route('season.destroy', $season->id) }}">
							@method('DELETE')
							@csrf
							<input type="submit" class="btn btn-danger btn-sm my-1" value="Hapus">
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection

