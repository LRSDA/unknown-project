@extends('layouts.menu')

@section('title', 'Tambah Season Baru')

@section('header', 'Season')

@section('back', route('season.index'))
	
@section('today_date')
	@php echo date('Y-m-d') @endphp
@endsection

@section('content')
	<form method="POST" action="{{ route('season.store') }}">
		@csrf
		@include('season.form')
	</form>
@endsection