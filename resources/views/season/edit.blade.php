@extends('layouts.menu')

@section('title')
	Ubah Season #{{ $season->id }}
@endsection

@section('header', 'Season')

@section('back', route('season.index'))

@section('value_type', $season->type)

@section('value_start', $season->start_date)

@section('value_end', $season->end_date)

@section('value_create', $season->create_date)

@section('value_status', $season->status)

@section('value_desc', $season->description)
	
@section('content')
	<form method="POST" action="{{ route('season.update', $season->id) }}">
		@method('PUT')
		@csrf
		@include('season.form')
	</form>
@endsection