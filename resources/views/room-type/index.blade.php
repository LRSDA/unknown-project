@extends('layouts.menu')

@section('title', 'Daftar Jenis Kamar')

@section('header', 'Jenis Kamar')

@section('create', route('room-type.create'))

@section('content')
	<table class="table table-sm table-striped table-bordered table-dark text-center">
		<thead class="thead-light">
			<tr>
				<th scope="col">Kode dan Nama</th>
				<th scope="col">Status</th>
				<th scope="col">Deskripsi</th>
				<th scope="col">Kapasitas</th>
				<th scope="col">Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($roomTypes as $roomType)
				<tr>
					<th scope="row" class="align-middle">{{ $roomType->code }}</th>
					@if(empty($roomType->status))
						<td class="align-middle" rowspan="2">-</td>
					@else
						<td class="align-middle" rowspan="2">{{ $roomType->status }}</td>
					@endif
					<td class="align-middle">{{ $roomType->description }}</td>
					<td class="align-middle" rowspan="2">{{ $roomType->capacity }} orang</td>
					<td class="align-middle" rowspan="2">
						<a class="btn btn-info btn-sm my-1" href="{{ route('room-type.edit', $roomType->id) }}" role="button">Ubah</a>
						<form method="POST" action="{{ route('room-type.destroy', $roomType->id) }}">
							@method('DELETE')
							@csrf
							<input type="submit" class="btn btn-danger btn-sm my-1" value="Hapus">
						</form>
					</td>
				</tr>
				<tr>
					<th scope="row" class="align-middle">{{ $roomType->name }}</th>
					<td class="align-middle">Rp. {{ number_format($roomType->price_idr,0,",",".") }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection