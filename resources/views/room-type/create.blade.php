@extends('layouts.menu')

@section('title', 'Tambah Jenis Kamar Baru')

@section('header', 'Jenis Kamar')

@section('back', route('room-type.index'))
	
@section('content')
	<form method="POST" action="{{ route('room-type.store') }}">
		@csrf
		@include('room-type.form')
	</form>
@endsection