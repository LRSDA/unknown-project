<div class="p-4 rounded bg-light">
	<h3>@yield('title')</h3>
	<div class="row">
		<div class="form-group col-lg-9">
			<label for="type_name">Nama Jenis Kamar</label>
			<input class="form-control" id="type_name" name="type_name" maxlength="25" placeholder="Maksimal 25 huruf" 
				@hasSection('value_name')
					value="@yield('value_name')"
				@endif
			>
		</div>
		<div class="form-group col-lg-3">
			<label for="type_code">Kode Jenis Kamar</label>
			<input class="form-control" id="type_code" name="type_code" maxlength="2" placeholder="Maksimal 2 huruf" 
				@hasSection('value_code')
					value=@yield('value_code')
				@endif
			>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-6">
			<label for="type_price">Harga Jenis Kamar (Rp.)</label>
			<input class="form-control" id="type_price" name="type_price" min="1"
				@hasSection('value_price')
					value=@yield('value_price')
				@endif
			>
		</div>
		<div class="form-group col-lg-6">
			<label for="type_cap">Kapasitas Jenis Kamar</label>
			<input class="form-control" id="type_cap" type="number" name="type_cap" min="1" max="3" placeholder="1 - 3" 
				@hasSection('value_cap')
					value=@yield('value_cap')
				@endif
			>
		</div>
	</div>
	<div class="form-group">
		<label for="type_desc">Deskripsi Jenis Kamar</label>
		<textarea class="form-control" id="type_desc" type="text" name="type_desc" maxlength="100" placeholder="Maksimal 100 huruf">@hasSection('value_desc')@yield('value_desc')@endif</textarea>
	</div>
	<input type="submit" class="btn btn-success" value="Unggah">
	<input type="reset" class="btn btn-secondary" value="Reset">
</div>