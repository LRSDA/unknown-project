@extends('layouts.menu')

@section('title')
	Ubah Jenis Kamar {{ $roomType->name }} ({{ $roomType->code }})
@endsection

@section('header', 'Jenis Kamar')

@section('back', route('room-type.index'))

@section('value_name', $roomType->name)

@section('value_code', $roomType->code)

@section('value_price', $roomType->price_idr)

@section('value_cap', $roomType->capacity)

@section('value_desc', $roomType->description)
	
@section('content')
	<form method="POST" action="{{ route('room-type.update', $roomType->id) }}">
		@method('PUT')
		@csrf
		@include('room-type.form')
	</form>
@endsection