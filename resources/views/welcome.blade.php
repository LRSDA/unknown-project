@extends('layouts.site')

@section('card-body')
    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url({{ URL::asset('img/superior.PNG') }});">
            <div class="carousel-caption d-none d-md-block transparent-dark">
              <h3>Superior</h3>
              <p>Wonderful Service and Comfortable Room .</p>
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url({{ URL::asset('img/double_deluxe.PNG') }})">
            <div class="carousel-caption d-none d-md-block transparent-dark">
              <h3>Double Deluxe</h3>
              <p>Unforgatable Wondeful Service and Room.</p>
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url({{ URL::asset('img/executive_deluxe.PNG') }})">
            <div class="carousel-caption d-none d-md-block transparent-dark">
              <h3>Executive Deluxe</h3>
              <p>You Will Not Be Able Find This Experiance Anywhere Else.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- Page Content -->
    <div class="container">

      <h1 class="my-4">Welcome to Grand Atma Hotel</h1>

      <!-- Features Section -->
      <div class="row">
        <div class="col-lg-6">
          <h2>About Us</h2>
          <p>Our Motto</p>
          <ul>
            <li>Serving with Heart</li>
            <li>Always be the Best</li>
            <li>Always Awesome</li>
            <li>Clean and Comfortable</li>
            <li>Understanding</li>
          </ul>
          <p>Contact:</p>
          <ul>
              <li>phone : 555666</li>
              <li>email : GrandAtma@Atma.ac.id</li>
          </ul>
        </div>
        <div class="col-lg-6">
          <img class="img-fluid rounded" src="{{ URL::asset('img/service.png') }}" alt="">
        </div>
      </div>
      <!-- /.row -->

      <hr>

      <!-- Call to Action Section -->
      <div class="row mb-4">
        <div class="col-md-8">
          
        </div>
        <div class="col-md-4">
          <a class="btn btn-lg btn-secondary btn-block" href="{{ route('side.room.search') }}">Reserve a Room Now</a>
        </div>
      </div>
@endsection
