@extends('layouts.menu')

@section('title')
	Ubah Fasilitas #{{ $facility->id }}
@endsection

@section('header', 'Fasilitas')

@section('back', route('facility.index'))

@section('value_name', $facility->name)

@section('value_price')
	@if(empty($facility->price_idr))
		"0"
	@else
		{{ $facility->price_idr }}
	@endif
@endsection

@section('value_qty')
	@if(empty($facility->quantity))
		"0"
	@else
		{{ $facility->quantity }}
	@endif
@endsection

@section('value_status', $facility->status)
	
@section('content')
	<form method="POST" action="{{ route('facility.update', $facility->id) }}">
		@method('PUT')
		@csrf
		@include('facility.form')
	</form>
@endsection