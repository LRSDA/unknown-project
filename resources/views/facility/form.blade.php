<div class="p-4 rounded bg-light">
	<h3>@yield('title')</h3>
	<div class="form-group">
		<label for="fac_name">Nama Fasilitas</label>
		<input class="form-control" id="fac_name" type="text" name="name" maxlength="50" 
			@hasSection('value_name')
				value="@yield('value_name')"
			@endif
		>
	</div>
	<div class="row">
		<div class="form-group col-md-7">
			<label for="fac_price">Harga (Rp.)</label>
			<input class="form-control" id="fac_price" type="number" name="price_idr" min="0" step="1000" 
				@hasSection('value_price')
					value=@yield('value_price')
				@endif
			>
		</div>
		<div class="form-group col-md-5">
			<label for="fac_qty">Kuantitas</label>
			<input class="form-control" id="fac_qty" type="number" name="quantity" min="0" 
				@hasSection('value_qty')
					value=@yield('value_qty')
				@endif
			>
		</div>
	</div>
	<div class="form-group">
		<label for="fac_status">Status</label>
		<!-- <input class="form-control" id="fac_status" type="text" name="status" 
			@hasSection('value_status')
				value="@yield('value_status')"
			@endif
		> -->
		<select class="form-control" id="fac_status" type="text" name="status" @hasSection('value_status')
				value="@yield('value_status')"
			@endif>
			
			<option value="Inactive" @hasSection('value_status')
				 	@if ( $facility->status == "Inactive" ) selected @endIf
				 @endif>
				Inactive
			</option>
			<option value="Active"  @hasSection('value_status')
					@if ( $facility->status == "Active" ) selected @endIf
				@endIf>
				Active
			</option>
		</select>
	</div>
	<input type="submit" class="btn btn-success" value="Unggah">
	<input type="reset" class="btn btn-secondary" value="Reset">
</div>