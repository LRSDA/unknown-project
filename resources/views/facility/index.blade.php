@extends('layouts.menu')

@section('title', 'Daftar Fasilitas')

@section('header', 'Fasilitas')

@section('create', route('facility.create'))

@section('content')
	<table class="table table-sm table-striped table-bordered table-dark text-center">
		<thead class="thead-light">
			<tr>
				<th scope="col">#</th>
				<th scope="col">Nama Fasilitas</th>
				<th scope="col">Kuantitas</th>
				<th scope="col">Harga</th>
				<th scope="col">Status</th>
				<th scope="col">Aksi</th>
			</tr>
		</thead>
		<tbody>
			@foreach($facilities as $facility)
				<tr>
					<th scope="row" class="align-middle">{{ $facility->id }}</th>
					<td class="align-middle">{{ $facility->name }}</td>
					<td class="align-middle">{{ $facility->quantity }}</td>
					@if ($facility->price_idr == 0)
						<td class="align-middle">-</td>
					@else
						<td class="align-middle">Rp. {{ number_format($facility->price_idr,0,",",".") }}</td>
					@endif
					<td class="align-middle">{{ $facility->status }}</td>
					<td>
						<a class="btn btn-info btn-sm my-1" href="{{ route('facility.edit', $facility->id) }}" role="button">Ubah</a>
						<form method="POST" action="{{ route('facility.destroy', $facility->id) }}">
							@method('DELETE')
							@csrf
							<input type="submit" class="btn btn-danger btn-sm my-1" value="Hapus">
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection

