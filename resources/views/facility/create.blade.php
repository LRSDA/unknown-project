@extends('layouts.menu')

@section('title', 'Tambah Fasilitas Baru')

@section('header', 'Fasilitas')

@section('back', route('facility.index'))
	
@section('content')
	<form method="POST" action="{{ route('facility.store') }}">
		@csrf
		@include('facility.form')
	</form>
@endsection