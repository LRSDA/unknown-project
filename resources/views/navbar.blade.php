<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand navbar-logo" href="{{ route('index') }}">
		<img class="navbar-logo" src="{{ URL::asset('img/grand_atma_hotels.png') }}">
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarItems" aria-controls="navbarItems" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarItems">
		<ul class="navbar-nav ml-auto">
			@guest 
				<li><a class="nav-link" href="{{ route('side.room.search') }}">Search For Rooms</a></li> 
				<li><a class="nav-link" href="{{ route('login') }}">Login</a></li> 
				<li><a class="nav-link" href="{{ route('register') }}">Register</a></li> 
			@else 
				<li><a class="nav-link" href="{{ route('side.room.search') }}">Search For Rooms</a></li> 
				<li class="nav-item dropdown"> 
					<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre> 
						{{ Auth::user()->name }} 
						<span class="caret"></span> 
					</a> 

					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"> 
						<a class="dropdown-item" href="{{ route('account.profile') }}">Akun Saya</a>

						<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> 
						Logout
						</a> 

						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> 
						@csrf 
						</form> 
					</div> 
				</li> 
			@endguest
		</ul>
	</div>
</nav>