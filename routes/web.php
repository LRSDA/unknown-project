<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); })->name('index');

Route::resource('facility','FacilityController');

Route::resource('room','RoomController');

Route::resource('room-type','RoomTypeController');

Route::resource('season','SeasonController');

Route::resource('price','PriceController');

Route::resource('user','AccountController');

Route::resource('report','ReportController');

Route::resource('reservation','ReservationController');

Route::post('login','AccountController@login')->name("login.submit");

Route::get('login','AccountController@showLogin')->name('login');

Route::post('logout','AccountController@logout')->name('logout');

Route::post('register','AccountController@createGuest')->name("guest.create.submit");

Route::get('register', function () {
	return view('register');
})->name('register');

Route::get('forgot', function () {
	return view('forgot');
})->name('forgot');

Route::post('forgot', 'AccountController@validateForgotPassword')->name('forgot.submit');

Route::post('reset', 'AccountController@resetPassword')->name('reset.submit');

Route::get('account/profile', 'AccountController@show')->name('account.profile');

Route::get('account/profile/edit', 'AccountController@edit')->name('account.profile.edit');

Route::get('account/reservation', 'ReservationController@show')->name('reservation.show');

Route::get('account/reservation/cancel/{reservation}', 'ReservationController@cancelReservation')->name('reservation.cancel');

Route::get('account/reservation/print/{reservation}', 'ReservationController@printReservation')->name('reservation.print');

Route::get('account/history', 'ReservationController@showHistory')->name('reservation.history');

Route::get('account/history/{reservation}', 'ReservationController@showHistoryDetail')->name('reservation.history.detail');

Route::get('account', function () {
	return redirect()->route('account.profile');
})->name('account.index');

Route::get('showcase/{name}', 'SideController@showRoomDetail')->name('side.showcase');

Route::get('orderRoom', 'SideController@createOrder')->name('side.room.order');

Route::get('searchRoom', 'SideController@showSearchRoom')->name('side.room.search');

Route::post('confirmation', 'SideController@showConfirmRoom')->name('side.room.confirm');

Route::post('payment', 'SideController@showPayment')->name('side.payment');

Route::post('storePayment', 'SideController@storePayment')->name('side.payment.store');

Route::post('getSearchRoom', 'SideController@searchRoomReservation')->name('side.room.getsearch');

Route::post('getYearIncome', 'ReportController@getYearIncome')->name('report.getYearIncome');

Route::post('getNewCustomer', 'ReportController@getNewCustomer')->name('report.getNewCustomer');

Route::post('getTopFive', 'ReportController@getTopFive')->name('report.getTopFive');

Route::post('getMonthIncome', 'ReportController@getMonthIncome')->name('report.getMonthIncome');

Route::post('getGuestType', 'ReportController@getGuestType')->name('report.getGuestType');