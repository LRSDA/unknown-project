function search(expand){
	let searchClue = $('input.searchClue').val().toLowerCase();
	let body = $('table tbody.implementSearch').html();
	if ($('tbody.implementSearch').attr('data-body') == null) {
		$('tbody.implementSearch').attr('data-body',body);
	}

	$('tbody.implementSearch').html($('tbody.implementSearch').attr('data-body'));


	if (searchClue != null) {
		let included = 0
		let result = $('tbody.implementSearch tr').filter(function() {
		    if($(this).find('td.search').length > 0 ){
		    	let result = false
		    	needcheck = $(this).find('td.search');
		    	needcheck.each(function(){
		    		if ($(this).attr('name').toLowerCase().indexOf(searchClue) > -1 ){
		    			result = true;
		    		}
		    		console.log($(this));
		    	})
		    	if (result){
		    		included = expand;
		    		return $(this);
		    	}
		    }
		    else if($(this).find('td.search').length == 0 && included > 0){
		    	included--;
		    	return $(this);
		    }

		});

		$('tbody.implementSearch').html(result);
	}
}

