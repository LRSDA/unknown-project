$(document).ready(function(){
	var today = new Date(),
    yyyy = today.getFullYear(),
    html = '';

	for (var  i = 2000; i <= yyyy; i++) {
		if( i == yyyy){
			html = html + '<option value='+i+' selected>' + i + '</option>';
		}
		else{
			html = html + '<option value='+i+'>' + i + '</option>';
		}
	};

	$('#year').html(html);

	let year = $('#year').val();
	let month = $('#month').val(); 
	let csrf = $('input[name="_token"]').val();
    $.ajax({
        type: 'POST',
        data: {year:year , _token: csrf},
        url: 'getTopFive',
        success: function (data) {
            if (data) {
               $('#tabel_report').html(data);
            }
        }
    }); 


	$('#report_type').change(function(){
		changeTable();
	})

	$('#year').change(function(){
		changeTable();
	})

	$('#month').change(function(){
		changeTable();
	})
})

function changeTable(){
	let year = $('#year').val();
	let month = $('#month').val(); 
	let csrf = $('input[name="_token"]').val();
	if( $('#report_type').val() == 1 ){
		//5 pelanggan terbanyak
		$('#year').parent().removeClass('hidden');
		$('#month').parent().addClass('hidden');
        $.ajax({
            type: 'POST',
            data: {year:year , _token: csrf},
            url: 'getTopFive',
            success: function (data) {
                if (data) {
                   $('#tabel_report').html(data);
                }
            }
        });  
	}
	else if( $('#report_type').val() == 2 ){
		//jumlah pelanggan baru perbulan
		$('#year').parent().removeClass('hidden');
		$('#month').parent().addClass('hidden');
		$.ajax({
            type: 'POST',
            data: {year:year , _token: csrf},
            url: 'getNewCustomer',
            success: function (data) {
                if (data) {
                   $('#tabel_report').html(data);
                }
            }
        });  
	}
	else if( $('#report_type').val() == 3 ){
		//pendapatan pertahun
		$('#year').parent().addClass('hidden');
		$('#month').parent().addClass('hidden');
		$.ajax({
            type: 'POST',
            data: {year:year , _token: csrf},
            url: 'getYearIncome',
            success: function (data) {
                if (data) {
                   $('#tabel_report').html(data);
                }
            }
        });  
	}
	else if( $('#report_type').val() == 4 ){
		//Pendapatan Perbulan per Jenis Tamu
		$('#year').parent().removeClass('hidden');
		$('#month').parent().addClass('hidden');
		$.ajax({
            type: 'POST',
            data: {year:year , _token: csrf},
            url: 'getMonthIncome',
            success: function (data) {
                if (data) {
                   $('#tabel_report').html(data);
                }
            }
        });  
	}
	else{
		// jumlah tamu per jenis kamar per bulan
		$('#year').parent().removeClass('hidden');
		$('#month').parent().removeClass('hidden');
		$.ajax({
            type: 'POST',
            data: {year:year, month:month , _token: csrf},
            url: 'getGuestType',
            success: function (data) {
                if (data) {
                   $('#tabel_report').html(data);
                }
            }
        });  
	}
}