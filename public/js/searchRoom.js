function goToOrder(value) {
    var url = value + window.location.search + "&" + $.param({ bed: $('#bed').val() });
    window.location.href = url;
}

function goToDetail(value) {
    let location = $('#location').val();
    let checkIn = $('#start_date').val();
    let checkOut = $('#end_date').val();
    let roomCount = $('#room_count').val();
    let duration = $('#duration').val();
    let guestAdult = $('#guest_adult').val();
    let guestChild = $('#guest_child').val();

    var url = "showcase/" + value + "?" + $.param({
            location : location,
            checkIn : checkIn,
            checkOut : checkOut,
            roomCount : roomCount,
            duration : duration,
            guestAdult : guestAdult,
            guestChild : guestChild,
            roomType : value
        });

    window.location.href = url;
}

function changeCheckOutDate(value){
	let checkIn = $('#start_date').val();
	let dateIn = new Date(checkIn);
	let dateOut = dateIn.addDays(value);

    var day = dateOut.getDate();
    var month = dateOut.getMonth() + 1;
    var year = dateOut.getFullYear();

    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;

    var checkOut = year + "-" + month + "-" + day;
	$('#end_date').val(checkOut);
}

Date.prototype.addDays = function (num) {
    var value = this.valueOf();
    value += 86400000 * num;
    return new Date(value);
}

$(document).ready(function(){
	var date = new Date();

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;

    var today = year + "-" + month + "-" + day;
	$('#start_date').val(today);
    $('#end_date').val(today);


    $('#searchRoom').submit(function(e){
        e.preventDefault();
        let datein = $('#start_date').val();
        let dateout = $('#end_date').val();
        let room_count = $('#room_count').val();
        let csrf = $('input[name="_token"]').val();


        $.ajax({
            type: 'POST',
            url: 'getSearchRoom',
            data: {datein: datein, dateout: dateout, room_count: room_count, _token: csrf},
            success: function (data) {
                if (data) {
                   $('#listRoom').html(data);
                }
            }
        });   
    });
});
