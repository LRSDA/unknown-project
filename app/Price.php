<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
   	protected $table = 'price';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'price_idr', 'status', 'room_type_id', 'season_id',
    ];
}
