<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityDescription extends Model
{
    protected $table = 'facility_description';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'description', 'name', 'room_type_id',
    ];
}
