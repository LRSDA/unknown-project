<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branch';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name', 'status', 'telephone','address', 
    ];
}
