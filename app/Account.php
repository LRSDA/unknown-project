<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class account extends Authenticatable
{
    use Notifiable;

    protected $table = 'account';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name', 'email', 'password','role', 'status', 'telephone', 'branch_id', 'address', 'identity_number',
    ];

    protected $hidden = [
    	'password'
    ];
}
