<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'room';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'code', 'floor', 'number','status', 'branch_id', 'room_type_id',
    ];
}
