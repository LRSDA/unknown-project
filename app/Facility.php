<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    protected $table = 'facility';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name', 'price_idr', 'quantity','status',
    ];
}
