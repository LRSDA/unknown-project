<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table = 'guest';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name', 'email', 'telephone', 'address', 'identity_number', 'first_reservation_date', 'institution_name',
    ];

}
