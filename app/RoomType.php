<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    protected $table = 'room_type';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'capacity', 'code', 'description','name', 'status', 'price_idr',
    ];
}
