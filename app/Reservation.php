<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = 'reservation';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'bail', 'check_in_date', 'check_out_date', 'code', 'deposit', 'gross_cost', 'guest_type', 'name_of_credit_card', 'number', 'number_of_adult', 'number_of_children', 'payment_date', 'reservation_date', 'status', 'branch_id', 'employee_id', 'guest_id'
    ];
}
