<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bed extends Model
{
    protected $table = 'bed';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'name', 'quantity', 'room_type_id', 
    ];
}
