<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $table = 'season';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'create_date', 'description', 'end_date','start_date', 'status', 'type', 'branch_id',
    ];
}
