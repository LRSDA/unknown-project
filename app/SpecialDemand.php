<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialDemand extends Model
{
    protected $table = 'special_demand';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'demand', 'reservation_id',
    ];
}
