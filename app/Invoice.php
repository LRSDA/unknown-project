<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'code', 'date', 'gross_cost', 'guest_type', 'net_cost', 'number', 'tax', 'employee_id', 'reservasi_id'
    ];
}
