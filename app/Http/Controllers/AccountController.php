<?php

namespace App\Http\Controllers;

use App\Account;
use App\Branch;
use App\Guest;
use App\Reservation;
use App\ReservedRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Database\QueryException;
use Auth;


class AccountController extends Controller
{
    public function __construct(){
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');;
        }
        $users = Account::all();
        $branches = Branch::all();
        $brancheArr = [];
        foreach ($branches as $key => $value) {
            $brancheArr[$value->id] = $value->name;
        }
        foreach ($users as$value) {

        }
       
        return view('user.index', ['users' => $users, 'branch' => $brancheArr]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $roles = ['Admin', 'Guest', 'GM', 'SM', 'HA', 'FO'];
        $branches = Branch::all();
        return view('user.create', ['roles' => $roles, 'branches' => $branches]);
    }

    public function createGuest(Request $request)
    {
        //
        request()->validate([
            'username' => 'required',
            'email' => 'required|email',
            'status' => 'nullable',
            'password' => 'required|same:password_confirmation',
            'password_confirmation' => 'required|same:password',
            'identity_number' => 'nullable',
            'telephone' => 'nullable',
            'address' => 'nullable',
        ]);
        Account::create([
            'name' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'identity_number' => $request['identity_number'],
            'status' => 'Active',
            'address' => $request['address'],
            'role' => 'Guest',
            'telephone' => $request['telephone'],
        ]);

        Guest::create([
            'name' => $request['username'],
            'email' => $request['email'],
            'identity_number' => $request['identity_number'],
            'address' => $request['address'],
            'telephone' => $request['telephone'],
        ]);

        return view('login');

    }

    public function createUser(Request $request)
    {
        //

        Account::create([
            'name' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'identity_number' => $request['identity_number'],
            'status' => 'Active',
            'address' => $request['address'],
            'role' => $request['role'],
            'telephone' => $request['telephone'],
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'username' => 'required',
            'email' => 'required|email',
            'role' => 'required',
            'status' => 'nullable',
            'password' => 'required|same:password_confirmation|min:4|max:15',
            'password_confirmation' => 'required|same:password|min:4|max:15',
            'identity_number' => 'nullable',
            'telephone' => 'nullable',
            'branch_id' => 'nullable',
            'address' => 'nullable',
        ]);
        AccountController::createUser($request);
        return redirect()->route('user.index')->with('success', 'User berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        if (session()->get('user_role')[0] == 'Guest') {
            $user = Account::find(session()->get('user_id'))->first();
            return view('account.profile', ['user' => $user]);
        }
        else {
            return redirect()->route('user.index');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $user)
    {
        if (session()->get('user_role')[0] == 'Guest') {
            $user = Account::find(session()->get('user_id'))->first();
            return view('account.profile-edit', ['user' => $user]);
        }
        $branches = Branch::all();
        $roles = ['Admin', 'Guest', 'GM', 'SM', 'HA', 'FO'];
        return view('user.edit', ['user' => $user, 'roles' => $roles, 'branches' => $branches]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $user)
    {
        if (session()->get('user_role')[0] == 'Guest') {
            request()->validate([
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'nullable|same:repassword',
                'repassword' => 'nullable|same:password',
                'telephone' => 'nullable',
                'address' => 'nullable',
            ]);

            if (!empty($request->get('password'))) {
                $user->password = Hash::make($request->get('password'));
            }
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->telephone = $request->get('telephone');
            $user->address = $request->get('address');

            $user->save();
            
            return redirect()->route('account.profile')->with('success', 'Profil berhasil diubah');
        }
        else {
            request()->validate([
                'username' => 'required',
                'email' => 'required|email',
                'role' => 'required',
                'status' => 'nullable',
                'identity_number' => 'nullable',
                'telephone' => 'nullable',
                'branch_id' => 'nullable',
                'address' => 'nullable',
            ]);

            $user->name = $request->get('username');
            $user->email = $request->get('email');
            $user->role = $request->get('role');
            $user->status = $request->get('status');
            $user->identity_number = $request->get('identity_number');
            $user->telephone = $request->get('telephone');
            $user->branch_id = $request->get('branch_id');
            $user->address = $request->get('address');

            $user->save();
            return redirect()->route('user.index')->with('success', 'User #'.$user->id.' berhasil diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $user)
    {
        try {
            $user->delete();
            return redirect()->route('user.index')->with('success', 'User #'.$user->id.' berhasil dihapus');
        }
        catch (QueryException $e) {
            return redirect()->route('user.index')->withErrors('Data User #'.$user->id.' masih tersambung dengan data lainnya.');
        }
    }

    public function login(Request $request) {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:4|max:15'
        ]);

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            $userData = Account::where(['email'=>$request->email])->first();
            
            session()->push('user_id',$userData->id);
            session()->push('user_role',$userData->role);
            session()->push('user_branch_id',$userData->branch_id);
            session()->push('name',$userData->name);
            session()->push('address',$userData->address);
            session()->push('email',$userData->email);
            session()->push('telephone',$userData->telephone);
            session()->push('identity_number',$userData->identity_number);
            
            return redirect()->intended('user');
        }
        else {
            return back()->withInput($request->only('email'));
        }

    }
    
    public function showLogin()
    {
        # code...
        return view('login');
    }

    public function logout(Request $request)
    {
        Auth::guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('index');
        # code...
    }

    public function showForgotPassword()
    {
        return view('forgot');
    }

    public function validateForgotPassword(Request $request)
    {
        request()->validate([
            'username' => 'required',
            'email' => 'required|email',
            'identity_number' => 'nullable',
            'telephone' => 'nullable',
        ]);
        $username = $request->username;
        $email = $request->email;
        $identity_number = $request->identity_number;
        $telephone = $request->telephone;

        $result = Account::where('name',$username)
                    ->where('email',$email)
                    ->where('identity_number',$identity_number)
                    ->where('telephone','like',$telephone)
                    ->first();

        if(count($result)){
            return view('reset', ['username' => $username,'email' => $email, 'identity_number' => $identity_number, 'telephone' => $telephone]);
        }
        else{
            return back()->withInput(['email']);
        }
    }

    public function resetPassword(Request $request)
    {
        if ($request->password == $request->password_confirmation) {
            $username = $request->username;
            $email = $request->email;
            $identity_number = $request->identity_number;
            $telephone = $request->telephone;

            $result = Account::where('name',$username)
                        ->where('email',$email)
                        ->where('identity_number',$identity_number)
                        ->where('telephone','like',$telephone)
                        ->first();
            $result->password = Hash::make($request['password']);
            $result->save();

            return view('login');
        }
        else {
            return back();
        }
        
    }

// public function validate(Array $data, Array $rule){
    //     Validator::make($data, $rule);
    // }
}
