<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
	 public function __construct(){
        $this->middleware('auth');       
    }


    public function index()
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');;
        }
        
        return view('report.index');
    }

    public function show()
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');;
        }
        
        return view('report.index');
    }

    public function getYearIncome(Request $request)
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');;
        }
        $rawdata =  DB::table('reservation')
                    ->select(DB::raw('Year(check_out_date) as year, sum(gross_cost) total'))
                    ->groupBy('year')
                    ->orderBy('year')
                    ->get();

        $data = [];
        $counter = 1;
        foreach ($rawdata as $value) {
    		$data[$counter] = $value;
    		$counter++;
        }

        return view('report.year',['datas' => $data]);
    }


    public function getMonthIncome(Request $request)
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');;
        }
        $year = $request->year;
        $rawdata =  DB::table('reservation')
                    ->select(DB::raw('guest_type, DATE_FORMAT(check_out_date, "%m") as month_num, DATE_FORMAT(check_out_date, "%M") as month, sum(gross_cost) as total'))
                    ->whereRaw('YEAR(check_out_date) = "'.$year.'" and guest_type = "Grup"')
                    ->groupBy('month_num')
                    ->orderBy('month_num')
                    ->get();
        $data = [];
        $counter = 1;
        foreach ($rawdata as $value) {
    		$data[$value->month_num]['Grup'] = $value->total;
            $data[$value->month_num]['Month'] = $value->month;
    		$data[$value->month_num]['Personal'] = 0;
    		$data[$value->month_num]['num'] = $counter;
    		$data[$value->month_num]['total'] = $data[$value->month_num]['Grup'];
    		$counter++;
        }
        $rawdata =  DB::table('reservation')
                    ->select(DB::raw('guest_type, DATE_FORMAT(check_out_date, "%m") as month_num, DATE_FORMAT(check_out_date, "%M") as month, sum(gross_cost) as total'))
                    ->whereRaw('YEAR(check_out_date) = "'.$year.'" and guest_type = "Personal"')
                    ->groupBy('month_num')
                    ->orderBy('month_num')
                    ->get();

        foreach ($rawdata as $value) {
    		$data[$value->month_num]['Personal'] = $value->total;
    		if( !empty($data[$value->month_num]['total']) ){
    			$data[$value->month_num]['total'] = $data[$value->month_num]['Personal'] + $data[$value->month_num]['Grup'];
    		}
    		else{
    			$data[$value->month_num]['total'] = $data[$value->month_num]['Personal'];
    		}
    		if(empty($data[$value->month_num]['num'])){
                $data[$value->month_num]['Month'] = $value->month;
    			$data[$value->month_num]['num'] = $counter;
    			$data[$value->month_num]['Grup'] = 0;
    			$counter++;
    		}
        }

        ksort($data);
       
        return view('report.month',['datas' => $data]);
    }

    public function getTopFive(Request $request)
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');;
        }

        $date = $request->year.'-01-01';
        $rawdata =  DB::table('reservation')
                    ->leftJoin('guest','guest.id','=','reservation.guest_id')
                    ->select(DB::raw('guest.name as name, count(reservation.id) as count_reservation, sum(reservation.gross_cost) as cost_reservation '))
                    ->whereRaw('reservation.check_out_date >= "'.$date.'" ')
                    ->groupBy('name')
                    ->orderBy('cost_reservation','desc')
                    ->get();

        $data = [];
        $counter =1;
        foreach ($rawdata as $value) {
        	if ($counter <= 5){
        		$data[$counter] = $value;
        		$counter++;
        	}
        	else{
        		break;
        	}
        }

        return view('report.top-customer',['datas' => $data]);
    }


    public function getNewCustomer(Request $request)
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');;
        }

        $datein = $request->year.'-01-01';
        $dateout = $request->year.'-12-31';
        $rawdata =  DB::table('guest')
                    ->select(DB::raw('DATE_FORMAT(first_reservation_date, "%m") as month_num, DATE_FORMAT(first_reservation_date, "%M") as month, count(id) as count_guest'))
                    ->whereRaw('first_reservation_date BETWEEN "'.$datein.'" AND "'.$dateout.'"')
                    ->groupBy('month_num')
                    ->orderBy('month_num')
                    ->get();
        $data = [];
        $counter = 1;
        foreach ($rawdata as $value) {
    		$data[$counter] = $value;
    		$counter++;
        }       
        return view('report.new-customer',['datas' => $data]);
    }

    public function getGuestType(Request $request)
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');;
        }
        $year = $request->year;
        $month = $request->month;
        $rawdata =  DB::table('reserved_room as rm')
			        ->leftJoin('room as r', 'r.id', '=', 'rm.room_id')
			        ->leftJoin('room_type as rt','r.room_type_id','=','rt.id')
			        ->leftJoin('reservation as rs','rm.reservation_id','=','rs.id')
                    ->select(DB::raw('rt.name as name, COUNT(rm.id) as grup'))
                    ->whereRaw('YEAR(rm.check_out_date) = '.$year.' AND MONTH(rm.check_out_date) = '.$month.' and rs.guest_type = "Grup" ')
                    ->groupBy('name')
                    ->get();
        $data = [];
        $counter = 1;
        foreach ($rawdata as $value) {
    		$data[$value->name]['Grup'] = $value->grup;
    		$data[$value->name]['Personal'] = 0;
    		$data[$value->name]['num'] = $counter;
    		$data[$value->name]['total'] = $data[$value->name]['Grup'];
    		$counter++;
        }       
        $rawdata =  DB::table('reserved_room as rm')
			        ->leftJoin('room as r', 'r.id', '=', 'rm.room_id')
			        ->leftJoin('room_type as rt','r.room_type_id','=','rt.id')
			        ->leftJoin('reservation as rs','rm.reservation_id','=','rs.id')
                    ->select(DB::raw('rt.name as name, COUNT(rm.id) as personal'))
                    ->whereRaw('YEAR(rm.check_out_date) = '.$year.' AND MONTH(rm.check_out_date) = '.$month.' and rs.guest_type = "Personal" ')
                    ->groupBy('name')
                    ->get();

        foreach ($rawdata as $value) {
    		$data[$value->name]['Personal'] = $value->personal;
    		if( !empty($data[$value->name]['total']) ){
    			$data[$value->name]['total'] = $data[$value->name]['Personal'] + $data[$value->name]['Grup'];
    		}
    		else{
    			$data[$value->name]['total'] = $data[$value->name]['Personal'];
    		}
    		if(empty($data[$value->name]['num'])){
    			$data[$value->name]['num'] = $counter;
    			$data[$value->name]['Grup'] = 0;
    			$counter++;
    		}
        }
        return view('report.guest-type',['datas' => $data]);
    }
}
