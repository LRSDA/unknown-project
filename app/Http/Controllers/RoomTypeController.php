<?php

namespace App\Http\Controllers;

use App\RoomType;
use Illuminate\Http\Request;

class RoomTypeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');;
        }
        $roomTypes = RoomType::all();
        return view('room-type.index', compact('roomTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('room-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'type_name' => 'required',
            'type_code' => 'required',
            'type_cap' => 'required|int|min:1|max:3',
            'type_price' => 'required|int|min:1',
            'type_desc' => 'required',
        ]);

        $typeName = $request->type_name;
        $typeCode = $request->type_code;
        RoomType::create([
            'capacity' => $request->get('type_cap'),
            'code' => strtoupper($request->get('type_code')),
            'description' => $request->get('type_desc'),
            'name' => $request->get('type_name'),
            'status' => 'Active',
            'price_idr' => $request->get('type_price'),
        ]);
        return redirect()->route('room-type.index')->with('success', "Jenis Kamar ".$typeName." (".$typeCode.") berhasil ditambahkan");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RoomType  $roomType
     * @return \Illuminate\Http\Response
     */
    public function show(RoomType $roomType)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RoomType  $roomType
     * @return \Illuminate\Http\Response
     */
    public function edit(RoomType $roomType)
    {
        return view('room-type.edit', ['roomType' => $roomType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RoomType  $roomType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoomType $roomType)
    {
        request()->validate([
            'type_name' => 'required',
            'type_code' => 'required',
            'type_cap' => 'required|int|min:1|max:3',
            'type_price' => 'required|int|min:1',
            'type_desc' => 'required',
        ]);

        $roomType->capacity = $request->get('type_cap');
        $roomType->code = strtoupper($request->get('type_code'));
        $roomType->description = $request->get('type_desc');
        $roomType->name = $request->get('type_name');
        $roomType->price_idr = $request->get('type_price');

        $roomType->save();
        return redirect()->route('room-type.index')->with('success', 'Jenis Kamar '.$roomType->name.' ('.$roomType->code.') berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RoomType  $roomType
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoomType $roomType)
    {
        try {
            $roomType->delete();
            return redirect()->route('room-type.index')->with('success', 'Jenis Kamar '.$roomType->name.' ('.$roomType->code.') berhasil dihapus');
        }
        catch (QueryException $e) {
            return redirect()->route('room-type.index')->withErrors('Data Jenis Kamar '.$roomType->name.' ('.$roomType->code.') masih tersambung dengan data lainnya.');
        }
    }
}
