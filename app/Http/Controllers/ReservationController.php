<?php

namespace App\Http\Controllers;

use App\Guest;
use App\Reservation;
use App\ReservedRoom;
use App\SpecialDemand;
use Illuminate\Http\Request;
use Auth;
use DateTime;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
        if (session()->get('user_role')[0] != 'Guest') {
            $reservations = Reservation::join('guest', 'reservation.guest_id', '=', 'guest.id')
                    ->select('reservation.*', 'guest.name')
                    ->get();
            return view('reservation.index', compact('reservations'));
        }
        else
            return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function show()
    {
        $guest = Guest::where('identity_number','=',session()->get('identity_number')[0])->first();
        if (session()->get('user_role')[0] == 'Guest') {
            $reservation = Reservation::where('guest_id', $guest->id)
                            ->where('check_in_date', '>=', date('Y-m-d'))
                            ->where('status', 'Active')
                            ->orderBy('check_in_date', 'desc')
                            ->first();

            if (empty($reservation))
                return view('reservation.guest');
            else {
                $rooms = ReservedRoom::where('reservation_id', $reservation->id)
                            ->where('reserved_room.status', 'Active')
                            ->leftJoin('room', 'reserved_room.room_id', '=', 'room.id')
                            ->leftJoin('room_type', 'room.room_type_id', '=', 'room_type.id')
                            ->get();
                $demands = SpecialDemand::where('reservation_id', $reservation->id)
                            ->get();

                $total = 0;
                $days = ((new DateTime($reservation->check_in_date))->diff(new DateTime($reservation->check_out_date)))->d + 1;
                foreach ($rooms as $room) {
                    $total = $total + ($room->price_idr * $days);
                }

                return view('reservation.guest', ['guest' => $guest, 'reservation' => $reservation, 'rooms' => $rooms, 'total' => $total, 'days' => $days, 'demands' => $demands]);
            }
        }
        else {
            return redirect()->back();
        }
    }

    public function showHistory()
    {
        if (session()->get('user_role')[0] == 'Guest') {
            $account = Guest::where('identity_number','=',session()->get('identity_number')[0])->first();
            $reservations = Reservation::where('guest_id', $account->id)
                            ->where('status', 'Active')
                            ->get();
            return view('reservation.history', ['account' => $account, 'reservations' => $reservations]);
        }
        else {
            return redirect()->route('user.index');
        }
    }

    public function showHistoryDetail(Reservation $reservation)
    {
        $guest = Guest::where('identity_number','=',session()->get('identity_number')[0])->first();
        if (session()->get('user_role')[0] == 'Guest') {
            if ($reservation->guest_id == $guest->id) {
                $name = $guest->name;
                $rooms = ReservedRoom::where('reservation_id', $reservation->id)
                            ->leftJoin('room', 'reserved_room.room_id', '=', 'room.id')
                            ->leftJoin('room_type', 'room.room_type_id', '=', 'room_type.id')
                            ->get();
                $demands = SpecialDemand::where('reservation_id', $reservation->id)
                            ->get();

                $total = 0;
                $days = ((new DateTime($reservation->check_in_date))->diff(new DateTime($reservation->check_out_date)))->d + 1;
                foreach ($rooms as $room) {
                    $total = $total + ($room->price_idr * $days);
                }
                return view('reservation.historyDetail', ['reservation' => $reservation, 'rooms' => $rooms, 'total' => $total, 'days' => $days, 'name' => $name, 'demands' => $demands]);
            }
            else
                return redirect()->route('reservation.history');
        }
        else {
            return redirect()->route('user.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */

    public function printReservation(Reservation $reservation)
    {
        $guest = Guest::where('identity_number','=',session()->get('identity_number')[0])->first();
        if (session()->get('user_role')[0] == 'Guest') {
            if ($reservation->guest_id == $guest->id) {
                if (empty($reservation))
                    return view('reservation.guest');
                else {
                    
                    $rooms = ReservedRoom::where('reservation_id', $reservation->id)
                                ->where('reserved_room.status', 'Active')
                                ->leftJoin('room', 'reserved_room.room_id', '=', 'room.id')
                                ->leftJoin('room_type', 'room.room_type_id', '=', 'room_type.id')
                                ->get();
                    $demands = SpecialDemand::where('reservation_id', $reservation->id)
                                ->get();

                    $total = 0;
                    $days = ((new DateTime($reservation->check_in_date))->diff(new DateTime($reservation->check_out_date)))->d + 1;
                    foreach ($rooms as $room) {
                        $total = $total + ($room->price_idr * $days);
                    }

                    return view('reservation.print', ['guest' => $guest, 'reservation' => $reservation, 'rooms' => $rooms, 'total' => $total, 'days' => $days, 'demands' => $demands]);
                }
            }
            else
                return redirect()->route('reservation.show');
        }
        else {
            return redirect()->route('user.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        $this->middleware('auth');
        if (session()->get('user_role')[0] != 'Guest') {
            $guests = Guest::select('id', 'name')
                        ->get();

            return view('reservation.edit', ['rsrv' => $reservation, 'guests' => $guests]);
        }
        else
            return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        $this->middleware('auth');
        if (session()->get('user_role')[0] != 'Guest') {
            request()->validate([
                'guest' => 'required|int',
                'check_in_date' => 'required|date|before_or_equal:check_out_date',
                'check_out_date' => 'required|date|after_or_equal:check_in_date',
                'number_of_adult' => 'required|int|min:1',
                'number_of_children' => 'nullable|int',
            ]);

            $reservedRooms = ReservedRoom::where('reservation_id', $reservation->id)
                                ->get();

            $reservation->guest_id = request()->get('guest');
            $reservation->check_in_date = request()->get('check_in_date');
            $reservation->check_out_date = request()->get('check_out_date');
            $reservation->number_of_adult = request()->get('number_of_adult');
            $reservation->number_of_children = request()->get('number_of_children');
            foreach ($reservedRooms as $room) {
                $room->check_in_date = request()->get('check_in_date');
                $room->check_out_date = request()->get('check_out_date');
                $room->save();
            }
            $reservation->save();

            return redirect()->route('reservation.index')->with('success', 'Reservasi '.$reservation->code.' berhasil diubah');
        }
        else
            return redirect()->back();
    }

    /**
     * Cancel the reservation (soft delete).
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function cancelReservation(Reservation $reservation)
    {
        if (session()->get('user_role')[0] == 'Guest') {
            if (!empty($reservation)) {
                $guest = Guest::where('identity_number','=',session()->get('identity_number')[0])->first();
                if ($reservation->guest_id == $guest->id) {
                    $reservation->status = 'Cancel';
                    $reservation->save();

                    $rooms = ReservedRoom::where('reservation_id', $reservation->id)
                                ->update(['status' => 'Cancel']);
                }
            }
            return redirect()->route('reservation.show');
        }
        else
            return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        $this->middleware('auth');
        if (session()->get('user_role')[0] != 'Guest') {
            try {
                $rooms = ReservedRoom::where('reservation_id', $reservation->id)
                        ->get();

                if (!empty($rooms)) {
                    foreach ($rooms as $room)
                        $room->delete();
                }
                $reservation->delete();
                return redirect()->route('reservation.index')->with('success', 'Reservasi '.$room->code.' berhasil dihapus');
            }
            catch (QueryException $e) {
                return redirect()->route('reservation.index')->withErrors('Reservasi '.$reservation->code.' masih tersambung dengan data lainnya.');
            }
        }
    }
}
