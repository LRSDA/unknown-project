<?php

namespace App\Http\Controllers;

use App\Room;
use App\RoomType;
use Illuminate\Http\Request;

class RoomController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');
        }
        $rooms = Room::join('room_type', 'room.room_type_id', '=', 'room_type.id')
                    ->select('room.*', 'room_type.name', 'room_type.description', 'room_type.price_idr')
                    ->get();
        return view('room.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('room.create', ['types' => RoomType::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'room_number' => 'required|int|min:1|max:25',
            'room_floor' => 'required|int|min:1|max:5',
            'room_type' => 'required',
        ]);

        $number = (int)$request->get('room_floor') * 1000 + (int)$request->get('room_number');
        $code = $number . RoomType::find($request['room_type'])->code;

        Room::create([
            'code' => $code,
            'floor' => $request->get('room_floor'),
            'number' => $request->get('room_number'),
            'status' => 'Active',
            'branch_id' => '1',
            'room_type_id' => $request->get('room_type'),
        ]);
        return redirect()->route('room.index')->with('success', 'Kamar ('.$code.') berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        return view('room.edit', ['types' => RoomType::all(), 'room' => $room]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        request()->validate([
            'room_number' => 'required|int|min:1|max:25',
            'room_floor' => 'required|int|min:1|max:5',
            'room_type' => 'required',
        ]);

        $request->get('email');
        $number = (int)$request->get('room_floor') * 1000 + (int)$request->get('room_number');
        $code = $number . RoomType::find($request['room_type'])->code;

        $room->code = $code;
        $room->floor = $request->get('room_floor');
        $room->number = $request->get('room_number');
        $room->status = 'Active';
        $room->branch_id = '1';
        $room->room_type_id = $request->get('room_type');

        $room->save();
        return redirect()->route('room.index')->with('success', 'Kamar ('.$room->code.') berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        try {
            $room->delete();
            return redirect()->route('room.index')->with('success', 'Kamar ('.$room->code.') berhasil dihapus');
        }
        catch (QueryException $e) {
            return redirect()->route('room.index')->withErrors('Data Kamar ('.$room->code.') masih tersambung dengan data lainnya.');
        }
    }
}
