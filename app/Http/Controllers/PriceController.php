<?php

namespace App\Http\Controllers;

use App\Price;
use App\RoomType;
use App\Season;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    public function __construct(){
        $this->middleware('auth');        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');
        }

        $prices = Price::join('room_type', 'price.room_type_id', '=', 'room_type.id')
                        ->join('season', 'price.season_id', '=', 'season.id')
                        ->select('price.*', 'room_type.name', 'room_type.code', 'season.description', 'season.type')
                        ->get();
        return view('price.index', compact('prices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roomTypes = RoomType::all();
        $seasons = Season::all();
        return view('price.create', ['roomTypes' => $roomTypes, 'seasons' => $seasons]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'price_idr' => 'required|int|min:1',
            'status' => 'required',
            'room_type_id' => 'required',
            'season_id' => 'required',
        ]);
        Price::create($request->all());
        return redirect()->route('price.index')->with('success', 'Tarif baru berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function show(Price $price)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function edit(Price $price)
    {
        $roomTypes = RoomType::all();
        $seasons = Season::all();
        return view('price.edit', ['roomTypes' => $roomTypes, 'seasons' => $seasons, 'price' => $price]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Price $price)
    {
        request()->validate([
            'price_idr' => 'required|int|min:1',
            'status' => 'required',
            'room_type_id' => 'required',
            'season_id' => 'required',
        ]);
        $price->update($request->all());
        return redirect()->route('price.index')->with('success', 'Tarif #'.$price->id.' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function destroy(Price $price)
    {
        try {
            $price->delete();
            return redirect()->route('price.index')->with('success', 'Tarif #'.$price->id.' berhasil dihapus');
        }
        catch (QueryException $e) {
            return redirect()->route('price.index')->withErrors('Data Tarif #'.$price->id.' masih tersambung dengan data lainnya.');
        }
    }
}
