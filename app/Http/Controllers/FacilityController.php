<?php

namespace App\Http\Controllers;

use App\Facility;
use Illuminate\Http\Request;

class FacilityController extends Controller
{
    public function __construct(){
        $this->middleware('auth');       
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');
        }

        $facilities = Facility::all();
        return view('facility.index', compact('facilities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('facility.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|max:50',
            'price_idr' => 'required|int|min:0',
            'quantity' => 'required|int|min:0',
            'status' => 'required',
        ]);
        Facility::create($request->all());
        return redirect()->route('facility.index')->with('success', 'Fasilitas '.$request->get('name').' berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function show(Facility $facility)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function edit(Facility $facility)
    {
        return view('facility.edit', ['facility' => $facility]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility)
    {
        request()->validate([
            'name' => 'required|max:50',
            'price_idr' => 'required|int|min:0',
            'quantity' => 'required|int|min:0',
            'status' => 'required',
        ]);
        $facility->update($request->all());
        return redirect()->route('facility.index')->with('success', 'Fasilitas #'.$facility->id.' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facility $facility)
    {
        try {
            $facility->delete();
            return redirect()->route('facility.index')->with('success', 'Fasilitas #'.$facility->id.' : '.$facility->name.' berhasil dihapus');
        }
        catch (QueryException $e) {
            return redirect()->route('facility.index')->withErrors('Data Fasilitas #'.$facility->id.' masih tersambung dengan data lainnya.');
        }
    }
}
