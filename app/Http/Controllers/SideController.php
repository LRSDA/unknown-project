<?php

namespace App\Http\Controllers;

use App\Bed;
use App\Branch;
use App\Facility;
use App\FacilityDescription;
use App\Guest;
use App\Reservation;
use App\ReservedRoom;
use App\Room;
use App\RoomType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class SideController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the room type.
     *
     * @return \Illuminate\Http\Response
     */
    public function showSearchRoom()
    {
        $roomTypes = RoomType::all();
        $branch = Branch::all();
        return view('side.search', ['roomTypes' => $roomTypes, 'branch' => $branch]);
    }

    /**
     * Display the form for payment
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showPayment(Request $request)
    {
        $code = 'P'.date('dmy');
        $latestCode = Reservation::where('code', 'like', $code.'%')
                    ->select('code')
                    ->get()
                    ->first();

        if (!empty($latestCode)) {
            $orderNumber = intval(substr($latestCode['code'], 8, 3)) + 1;
            if ($orderNumber < 10)
                $code = $code.'-00'.$orderNumber;
            else if ($orderNumber < 100)
                $code = $code.'-0'.$orderNumber;
            else
                $code = $code.'-'.$orderNumber;
        }
        else {
            $code = $code.'-001';
        }

        $datas = $request->all();
        $roomType = RoomType::where('name', '=', $request->get('type'))
                        ->select('*')
                        ->get()
                        ->first();
        return view('side.payment', ['datas' => $datas, 'code' => $code, 'roomType' => $roomType]);
    }

    /**
     * Show the form for order the room
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createOrder(Request $request)
    {
       
        if(Auth::check()){
            $datas = $request->all();

            if (session()->get('user_role')[0] != 'Guest') {
                echo "fail";
                return redirect()->route('user.index');
            }

            $user = [];
            $user['name'] = session()->get('name')[0];
            $user['address'] = session()->get('address')[0];
            $user['email'] = session()->get('email')[0];
            $user['telephone'] = session()->get('telephone')[0];
            $user['identity_number'] = session()->get('identity_number')[0];

            if (empty($request->get('bed'))) { 
                return redirect()->route('side.room.search');
            }
            else { 
                return view('side.order', ['datas' => $datas, 'user_data' => $user]);
            }
        }
        else{
            echo "fail";
            return redirect('/login');
        }
    }


    /**
     * Store new payment in reservation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePayment(Request $request)
    {
        request()->validate([
            'card_number' => 'nullable|int|max:16|min:16',
            'valid_date' => 'nullable|date',
            'cvv' => 'nullable|int|max:3|min:3',
            'card_name' => 'nullable',
        ]);

        $guest = Guest::where('identity_number','=',request()->get('id_number'))->first();
        if (empty($guest)) {
            $guest = Guest::create([
                'name' => request()->get('name'),
                'email' => request()->get('email'),
                'identity_number' => request()->get('id_number'),
                'address' => request()->get('address'),
                'telephone' => request()->get('telephone'),
            ]);
        }
        

        if (empty(request()->get('card_number'))) {
            $reserve = Reservation::create([
                'bail' => request()->get('deposit'),
                'check_in_date' => date_format(date_create(request()->get('checkIn')), "Y-m-d"),
                'check_out_date' => date_format(date_create(request()->get('checkOut')), "Y-m-d"),
                'code' => request()->get('code'),
                'deposit' => request()->get('deposit'),
                'gross_cost' => request()->get('grossCost'),
                'guest_type' => 'Personal',
                'number' => request()->get('roomCount'),
                'number_of_adult' => request()->get('guestAdult'),
                'number_of_children' => request()->get('guestChild'),
                'payment_date' => date('Y-m-d'),
                'reservation_date' => date('Y-m-d'),
                'status' => 'Active',
                'branch_id' => request()->get('location'),
                'guest_id' => $guest->id,
            ]);


            $datein = date_format(date_create(request()->get('checkIn')), "Y-m-d");
            $dateout = date_format(date_create(request()->get('checkOut')), "Y-m-d");
            $count =request()->get('roomCount');

            $subsub =  DB::table('reserved_room')
                    ->select(DB::raw('reserved_room.room_id as id'))
                    ->whereRaw('( "'.$datein.'" BETWEEN reserved_room.check_in_date AND reserved_room.check_out_date) OR ( "'.$dateout.'"BETWEEN reserved_room.check_in_date AND reserved_room.check_out_date)');
                    
            $sub =  DB::table('room')
                ->select(DB::raw('*'))
                ->whereRaw(' room.id NOT IN ('.$subsub->toSql().')  AND room.room_type_id = '.request()->get('roomType'))
                ->limit($count)
                ->get();

            foreach ($sub as $value) {
                ReservedRoom::create([
                    'check_in_date' => date_format(date_create(request()->get('checkIn')), "Y-m-d"),
                    'check_out_date' => date_format(date_create(request()->get('checkOut')), "Y-m-d"),
                    'status' => 'Active',
                    'branch_id' => request()->get('location'),
                    'reservation_id' => $reserve->id ,
                    'room_id' => $value->id,
                    'room_type_id' => $value->room_type_id,
                ]);
            }

            if (empty($guest->first_reservation_date)) {
                Guest::find($guest->id)->update(['first_reservation_date' => date('Y-m-d')]);
            }
        }
        else {
            $reserve = Reservation::create([
                'bail' => request()->get('dp_idr'),
                'check_in_date' => date_format(date_create(request()->get('checkIn')), "Y-m-d"),
                'check_out_date' => date_format(date_create(request()->get('checkOut')), "Y-m-d"),
                'code' => request()->get('code'),
                'deposit' => request()->get('deposit'),
                'gross_cost' => request()->get('grossCost'),
                'guest_type' => 'Personal',
                'name_of_credit_card' => request()->get('card_name'),
                'number' => request()->get('roomCount'),
                'number_of_adult' => request()->get('guestAdult'),
                'number_of_children' => request()->get('guestChild'),
                'number_of_credit_card' => request()->get('card_number'),
                'payment_date' => date('Y-m-d'),
                'reservation_date' => date('Y-m-d'),
                'status' => 'Active',
                'branch_id' => request()->get('location'),
                'guest_id' => $guest->id,
            ]);


            $datein = date_format(date_create(request()->get('checkIn')), "Y-m-d");
            $dateout = date_format(date_create(request()->get('checkOut')), "Y-m-d");
            $count =request()->get('roomCount');

            $subsub =  DB::table('reserved_room')
                    ->select(DB::raw('reserved_room.room_id as id'))
                    ->whereRaw('( "'.$datein.'" BETWEEN reserved_room.check_in_date AND reserved_room.check_out_date) OR ( "'.$dateout.'"BETWEEN reserved_room.check_in_date AND reserved_room.check_out_date)');
                    
            $sub =  DB::table('room')
                ->select(DB::raw('*'))
                ->whereRaw(' room.id NOT IN ('.$subsub->toSql().')  AND room.room_type_id = '.request()->get('roomType'))
                ->limit($count)
                ->get();
           

            foreach ($sub as $value) {
                ReservedRoom::create([
                    'check_in_date' => date_format(date_create(request()->get('checkIn')), "Y-m-d"),
                    'check_out_date' => date_format(date_create(request()->get('checkOut')), "Y-m-d"),
                    'status' => 'Active',
                    'branch_id' => request()->get('location'),
                    'reservation_id' => $reserve->id ,
                    'room_id' => $value->id,
                    'room_type_id' => $value->room_type_id,
                ]);
            }

            if (empty($guest->first_reservation_date)) {
                Guest::find($guest->id)->update(['first_reservation_date' => date('Y-m-d')]);
            }
        }
        
        $roomTypes = RoomType::all();
        $branch = Branch::all();
        return view('side.search', ['roomTypes' => $roomTypes, 'branch' => $branch]);
    }

    /**
     * Display the specified room type.
     *
     * @param  String  $name
     * @return \Illuminate\Http\Response
     */
    public function showRoomDetail(Request $request, $name)
    {
        $roomType = RoomType::where('name', $name)->get()->first();
        $facility_description= FacilityDescription::where('room_type_id', $roomType->id)->get();
        $facility = DB::table('facility')
                    ->leftJoin('detail_facility', 'facility.id', '=', 'detail_facility.facility_id')
                    ->select('facility.*')
                    ->where('room_type_id', $roomType->id)
                    ->get();
        $beds = Bed::where('room_type_id', $roomType->id)->get();
        return view('side.showcase', ['facility_description' => $facility_description, 'type' => $roomType, 'beds' => $beds, 'facility'=>$facility]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showConfirmRoom(Request $request)
    {
        try {
            request()->validate([
                'location' => 'required',
                'guestAdult' => 'required|int|min:1',
                'guestChild' => 'nullable|int',
                'bed' => 'required',
                'name' => 'required|min:1|max:25',
                'id_number' => 'required|min:8|max:16',
                'telephone' => 'nullable',
                'address' => 'nullable|max:100',
                'email' => 'nullable',
                'demand' => 'nullable',
            ]);

            $datas = $request->all();            
            $price_idr = RoomType::where('name', '=', $request->get('roomType'))
                            ->select('price_idr')
                            ->get()
                            ->first();

            return view('side.confirm', ['datas' => $datas, 'price_idr' => $price_idr]);
        }
        catch (Exception $e) {
            return redirect()->back()->withErrors()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FacilityDescription  $facilityDescription
     * @return \Illuminate\Http\Response
     */
    public function edit(FacilityDescription $facilityDescription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FacilityDescription  $facilityDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FacilityDescription $facilityDescription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FacilityDescription  $facilityDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(FacilityDescription $facilityDescription)
    {
        //
    }

    public function searchRoomReservation(Request $request){
        $datein = date('Y-m-d',strtotime($request->post('datein')));
        $dateout = date('Y-m-d',strtotime($request->post('dateout')));
        $cRoom = $request->post('room_count');
        $subsub =  DB::table('reserved_room')
                    ->select(DB::raw('reserved_room.room_id as id'))
                    ->whereRaw('( "'.$datein.'" BETWEEN reserved_room.check_in_date AND reserved_room.check_out_date) OR ( "'.$dateout.'"BETWEEN reserved_room.check_in_date AND reserved_room.check_out_date)');
                    
        $sub =  DB::table('room')
                ->select(DB::raw('room.room_type_id as type_id , count(room.room_type_id) as count_room '))
                ->whereRaw(' room.id NOT IN ('.$subsub->toSql().')')
                ->groupBy('room.room_type_id');

        $query = DB::table(DB::raw('('.$sub->toSql().') as temp'))
            ->select('type_id')
            ->where('temp.count_room','>=',$cRoom);
            
        // echo $sub->toSql();
        // echo $query ;
        $result  =  $query->get()->toArray();

        $result = array_column($result, 'type_id');
        $roomTypes = RoomType::whereIn('id',$result)->get();
        $branch = Branch::all();
        echo view('side.ajaxSearch', ['roomTypes' => $roomTypes, 'branch' => $branch]);
    }
}
