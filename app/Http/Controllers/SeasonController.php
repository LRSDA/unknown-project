<?php

namespace App\Http\Controllers;

use App\Season;
use Illuminate\Http\Request;

class SeasonController extends Controller
{
    public function __construct(){
        $this->middleware('auth');        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->get('user_role')[0] == 'Guest') {
            return redirect()->route('index');
        }

        $seasons = Season::all();
        return view('season.index', compact('seasons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = ['Normal', 'Promo', 'High'];
        return view('season.create', ['types' => $types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'type' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'create_date' => 'required|date|before_or_equal:today',
            'status' => 'required',
            'description' => 'required',
        ]);
        Season::create($request->all());
        return redirect()->route('season.index')->with('success', 'Season baru berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function show(Season $season)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function edit(Season $season)
    {
        $types = ['Normal', 'Promo', 'High'];
        return view('season.edit', ['season' => $season, 'types' => $types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Season $season)
    {
        request()->validate([
            'type' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'create_date' => 'required|date|before_or_equal:today',
            'status' => 'required',
            'description' => 'required',
        ]);
        $season->update($request->all());
        return redirect()->route('season.index')->with('success', 'Season #'.$season->id.' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Season  $season
     * @return \Illuminate\Http\Response
     */
    public function destroy(Season $season)
    {
        try {
            $season->delete();
            return redirect()->route('season.index')->with('success', 'Season #'.$season->id.' berhasil dihapus');
        }
        catch (QueryException $e) {
            return redirect()->route('season.index')->withErrors('Data Season '.$season->id.' masih tersambung dengan data lainnya.');
        }
    }
}
