<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservedRoom extends Model
{
    protected $table = 'reserved_room';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'check_in_date', 'check_out_date', 'status', 'branch_id', 'reservation_id', 'room_id', 'room_type_id',
    ];
}
