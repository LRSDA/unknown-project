-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2018 at 03:01 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sigah_8224_8532_8551`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `identity_number` decimal(19,2) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `address`, `email`, `identity_number`, `name`, `password`, `role`, `status`, `telephone`, `branch_id`) VALUES
(1, 'Jalan Jogokariyan Nomor 2', 'anggreini@gmail.com', '150708224.00', 'Anggreini', 'anggreini123', 'Guest', 'Active', '0821276428646', NULL),
(2, 'Jalan Babarsari Nomor 34', 'retno@gmail.com', '150608224.00', 'Retno', 'retno123', 'SM', 'Active', '0821276428646', 1),
(3, 'Jalan Wonosari Nomor 12', 'dyah@gmail.com', '150508224.00', 'Dyah', 'dyah123', 'FO', 'Active', '0821276428646', 1),
(4, 'Jalan Solo Nomor 56', 'ayuningtias@gmail.com', '150408224.00', 'Ayuningtias', 'ayuningtias', 'FO', 'Active', '0821276428646', 2),
(5, 'Jalan Magelang Nomor 66', 'tias@gmail.com', '150308224.00', 'Tias', 'tias', 'SM', 'Active', '0821276428646', 1),
(6, 'Jalan Seturan Nomor 44', 'desry@gmail.com', '150708532.00', 'Desry', 'desry123', 'CEO', 'Active', '0821276428646', NULL),
(7, 'Jalan Selokan Mataram Nomor 10', 'natalia@gmail.com', '150608532.00', 'Natalia', 'natalia123', 'Admin', 'Active', '0821276428646', NULL),
(8, 'Jalan Janti Nomor 25', 'ernandy@gmail.com', '150708551.00', 'Ernandy', 'ernandy123', 'SM', 'Active', '0821276428646', 2),
(9, 'Jalan Brigjen Katamso Nomor 13', 'magat@gmail.com', '150608551.00', 'Magat', 'magat123', 'FO', 'Active', '0821276428646', 1),
(10, 'Jalan Kaliurang Nomor 14', 'julita@gmail.com', '15058551.00', 'Julita', 'julita123', 'SM', 'Active', '0821276428646', 2);

-- --------------------------------------------------------

--
-- Table structure for table `additional_facility`
--

CREATE TABLE `additional_facility` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `price_idr` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `facility_id` int(11) DEFAULT NULL,
  `reservation_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `additional_facility`
--

INSERT INTO `additional_facility` (`id`, `date`, `price_idr`, `quantity`, `facility_id`, `reservation_id`) VALUES
(1, '2018-01-10', 10000, 1, 1, 1),
(2, '2018-02-10', 25000, 1, 2, 2),
(3, '2018-03-10', 75000, 1, 3, 3),
(4, '2018-04-10', 20000, 1, 4, 4),
(5, '2018-05-10', 150000, 1, 5, 5),
(6, '2018-06-10', 100000, 1, 6, 6),
(7, '2018-07-10', 100000, 1, 7, 7),
(8, '2018-08-10', 200000, 1, 8, 8),
(9, '2018-09-10', 10000, 1, 9, 9),
(10, '2018-10-10', 25000, 1, 10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `bed`
--

CREATE TABLE `bed` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `room_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bed`
--

INSERT INTO `bed` (`id`, `name`, `quantity`, `room_type_id`) VALUES
(1, 'Double', 1, 1),
(2, 'Twin', 1, 1),
(3, 'Double', 1, 2),
(4, 'Twin', 1, 2),
(5, 'King', 1, 3),
(6, 'King', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `address`, `name`, `status`, `telephone`) VALUES
(1, 'Jalan Malioboro Nomor 1', 'Yogyakarta', 'Active', '+6282123456789'),
(2, 'Jalan Asia Afrika Nomor 2', 'Bandung', 'Active', '+6282987654321');

-- --------------------------------------------------------

--
-- Table structure for table `detail_facility`
--

CREATE TABLE `detail_facility` (
  `id` int(11) NOT NULL,
  `facility_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_facility`
--

INSERT INTO `detail_facility` (`id`, `facility_id`, `room_type_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 1, 2),
(6, 2, 2),
(7, 3, 2),
(8, 4, 2),
(9, 1, 3),
(10, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `detail_reservation`
--

CREATE TABLE `detail_reservation` (
  `id` int(11) NOT NULL,
  `bed_type` varchar(255) DEFAULT NULL,
  `price_idr` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price_id` int(11) DEFAULT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_reservation`
--

INSERT INTO `detail_reservation` (`id`, `bed_type`, `price_idr`, `quantity`, `price_id`, `reservation_id`, `room_type_id`) VALUES
(1, 'King', '500000', 2, 1, 1, 1),
(2, 'Double', '500000', 1, 1, 2, 2),
(3, 'King', '500000', 2, 1, 3, 2),
(4, 'Double', '500000', 2, 1, 4, 3),
(5, 'King', '500000', 2, 1, 5, 1),
(6, 'King', '500000', 1, 1, 5, 3),
(7, 'King', '500000', 1, 1, 6, 2),
(8, 'King', '500000', 1, 1, 7, 3),
(9, 'King', '500000', 1, 1, 8, 1),
(10, 'Double', '500000', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price_idr` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `name`, `price_idr`, `quantity`, `status`) VALUES
(1, 'AC', 0, 0, 'Active'),
(2, 'Air minum', 0, 0, 'Active'),
(3, 'Jubah mandi', 0, 0, 'Active'),
(4, 'Minibar', 0, 0, 'Active'),
(5, 'Extra bed', 150000, 50, 'Active'),
(6, 'Message/orang', 75000, 0, 'Active'),
(7, 'Dinner package/orang', 100000, 0, 'Active'),
(8, 'Minibar/minuman', 2700000, 0, 'Active'),
(9, 'Meeting Room Full Day/orang', 200000, 5, 'Active'),
(10, 'Laundry reguler/potong', 10000, 0, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `facility_description`
--

CREATE TABLE `facility_description` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `room_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility_description`
--

INSERT INTO `facility_description` (`id`, `description`, `name`, `room_type_id`) VALUES
(1, 'Ruang duduk terpisah', 'Layout', 1),
(2, 'WiFi Gratis', 'Internet', 1),
(3, 'Televisi LCD', 'Hiburan', 1),
(4, 'Pembuat kopi/teh, air minum kemasan gratis, termasuk sarapan', 'Makan Minum', 1),
(5, 'Seprai kualitas premium dan gorden/tirai kedap cahaya', 'Untuk Tidur', 1),
(6, 'Kamar mandi pribadi, jubah mandi, dan sandal', 'Kamar Mandi', 1),
(7, 'Telepon dan tempat tidur lipat/tambahan tersedia', 'Kemudahan', 1),
(8, 'AC dan layanan pembenahan kamar harian', 'Kenyamanan', 1),
(9, 'Merokok/Dilarang Merokok', 'Keamanan', 1),
(10, 'Ruang duduk terpisah', 'Layout', 2);

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE `guest` (
  `id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_reservation_date` date DEFAULT NULL,
  `identity_number` decimal(19,2) DEFAULT NULL,
  `institution_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest`
--

INSERT INTO `guest` (`id`, `address`, `email`, `first_reservation_date`, `identity_number`, `institution_name`, `name`, `telephone`) VALUES
(1, 'Jalan Babarsari Nomor 1', 'ariana@gmail.com', '2018-01-10', '150708224.00', 'UAJY', 'Ariana', '081324248824'),
(2, 'Jalan Solo Nomor 2', 'thalia@gmail.com', '2018-02-10', '150608224.00', 'UBJY', 'Thalia', '081324348824'),
(3, 'Jalan Bantul Nomor 3', 'yanisha@gmail.com', '2018-03-10', '150508224.00', 'UCJY', 'Yanisha', '081324448824'),
(4, 'Jalan Magelang Nomor 4', 'talita@gmail.com', '2018-04-10', '150408224.00', 'UDJY', 'Talita', '081326548824'),
(5, 'Jalan Wates Nomor 5', 'debora@gmail.com', '2018-05-10', '150308224.00', 'UEJY', 'Debora', '081327248824'),
(6, 'Jalan Kemuning Nomor 6', 'dissa@gmail.com', '2018-06-10', '150708532.00', 'UFJY', 'Dissa', '081328248824'),
(7, 'Jalan Nusa Indah Nomor 7', 'ratih@gmail.com', '2018-07-10', '150608532.00', 'UGJY', 'Ratih', '081329248824'),
(8, 'Jalan Seturan Nomor 8', 'candra@gmail.com', '2018-08-10', '150708551.00', 'UHJY', 'Candra', '081321024882'),
(9, 'Jalan Janti Nomor 9', 'nisa@gmail.com', '2018-09-10', '150608551.00', 'UIJY', 'Nisa', '081321124824'),
(10, 'Jalan Monjali Nomor 10', 'fitri@gmail.com', '2018-10-10', '15058551.00', 'UJJY', 'Fitri', '081324128824');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `gross_cost` double DEFAULT NULL,
  `guest_type` varchar(255) DEFAULT NULL,
  `net_cost` double DEFAULT NULL,
  `number` int(11) NOT NULL,
  `tax` double DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `reservasi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `code`, `date`, `gross_cost`, `guest_type`, `net_cost`, `number`, `tax`, `employee_id`, `reservasi_id`) VALUES
(1, 'P300118-024', '2018-03-06', 500000, 'Personal', 550000, 1, 50000, NULL, 1),
(2, 'P300118-025', '2018-03-06', 500000, 'Personal', 550000, 2, 50000, NULL, 2),
(3, 'P300118-026', '2018-03-06', 500000, 'Personal', 550000, 3, 50000, NULL, 3),
(4, 'P300118-027', '2018-03-06', 500000, 'Personal', 550000, 4, 50000, NULL, 4),
(5, 'G300118-028', '2018-03-06', 500000, 'Grup', 550000, 5, 50000, 2, 5),
(6, 'G300118-029', '2018-03-06', 500000, 'Grup', 550000, 6, 50000, 2, 6),
(7, 'G300118-010', '2018-03-06', 500000, 'Grup', 550000, 7, 50000, 2, 7),
(8, 'G300118-011', '2018-03-06', 500000, 'Grup', 550000, 8, 50000, 2, 8),
(9, 'G300118-012', '2018-03-06', 500000, 'Grup', 550000, 9, 50000, 2, 9),
(10, 'G300118-013', '2018-03-06', 500000, 'Grup', 550000, 10, 50000, 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE `price` (
  `id` int(11) NOT NULL,
  `price_idr` double DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `season_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price`
--

INSERT INTO `price` (`id`, `price_idr`, `status`, `room_type_id`, `season_id`) VALUES
(1, 700000, 'Active', 1, 1),
(2, 600000, 'Active', 1, 2),
(3, 500000, 'Active', 1, 3),
(4, 700000, 'Active', 1, 4),
(5, 600000, 'Active', 1, 5),
(6, 500000, 'Active', 1, 6),
(7, 700000, 'Active', 1, 7),
(8, 600000, 'Active', 1, 8),
(9, 500000, 'Active', 1, 9),
(10, 700000, 'Active', 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `bail` double NOT NULL,
  `check_in_date` date NOT NULL,
  `check_out_date` date NOT NULL,
  `code` varchar(255) NOT NULL,
  `deposit` double NOT NULL,
  `gross_cost` double NOT NULL,
  `guest_type` varchar(255) NOT NULL,
  `name_of_credit_card` varchar(255) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `number_of_adult` int(11) NOT NULL,
  `number_of_children` int(11) NOT NULL,
  `number_of_credit_card` decimal(19,2) DEFAULT NULL,
  `payment_date` date NOT NULL,
  `reservation_date` date NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `guest_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id`, `bail`, `check_in_date`, `check_out_date`, `code`, `deposit`, `gross_cost`, `guest_type`, `name_of_credit_card`, `number`, `number_of_adult`, `number_of_children`, `number_of_credit_card`, `payment_date`, `reservation_date`, `status`, `branch_id`, `employee_id`, `guest_id`) VALUES
(1, 300000, '2018-01-10', '2018-02-10', 'P180110-001', 300000, 150000, 'Personal', 'Anggreini', 1, 1, 0, '150708224.00', '2018-01-10', '2018-01-10', 'Active', 1, NULL, 1),
(2, 300000, '2018-02-10', '2018-03-10', 'P180210-001', 300000, 150000, 'Personal', 'Retno', 1, 1, 0, '150608224.00', '2018-02-10', '2018-02-10', 'Active', 2, NULL, 3),
(3, 300000, '2018-03-10', '2018-04-10', 'G180310-001', 300000, 150000, 'Grup', 'Dyah', 1, 1, 0, '150508224.00', '2018-03-10', '2018-03-10', 'Active', 1, 2, 1),
(4, 300000, '2018-04-10', '2018-05-10', 'P180410-001', 300000, 150000, 'Personal', 'Ayuningtias', 1, 1, 0, '150408224.00', '2018-04-10', '2018-04-10', 'Active', 1, NULL, 1),
(5, 300000, '2018-05-10', '2018-06-10', 'G180510-001', 300000, 150000, 'Grup', 'Tias', 1, 1, 0, '150308224.00', '2018-05-10', '2018-05-10', 'Active', 1, 2, 1),
(6, 300000, '2018-06-10', '2018-07-10', 'P180610-001', 300000, 150000, 'Personal', 'Desry', 1, 1, 0, '150708532.00', '2018-06-10', '2018-06-10', 'Active', 1, NULL, 1),
(7, 300000, '2018-07-10', '2018-08-10', 'P180710-001', 300000, 150000, 'Personal', 'Natalia', 1, 1, 0, '150608532.00', '2018-07-10', '2018-07-10', 'Active', 2, NULL, 3),
(8, 300000, '2018-08-10', '2018-09-10', 'G180810-001', 300000, 150000, 'Grup', 'Ernandy', 1, 1, 0, '150708551.00', '2018-08-10', '2018-08-10', 'Active', 2, 8, 3),
(9, 300000, '2018-09-10', '2018-10-10', 'G180910-001', 300000, 150000, 'Grup', 'Magat', 1, 1, 0, '150608551.00', '2018-09-10', '2018-09-10', 'Active', 1, 2, 1),
(10, 300000, '2018-10-10', '2018-11-10', 'P181010-001', 300000, 150000, 'Personal', 'Julita', 1, 1, 0, '150508551.00', '2018-10-10', '2018-10-10', 'Active', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reserved_room`
--

CREATE TABLE `reserved_room` (
  `id` int(11) NOT NULL,
  `check_in_date` date NOT NULL,
  `check_out_date` date NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reserved_room`
--

INSERT INTO `reserved_room` (`id`, `check_in_date`, `check_out_date`, `status`, `branch_id`, `reservation_id`, `room_id`, `room_type_id`) VALUES
(1, '2018-02-28', '2018-03-28', 'Active', 1, 1, 1, 1),
(2, '2018-03-05', '2018-03-06', 'Active', 1, 2, 2, 1),
(3, '2018-03-05', '2018-03-06', 'Active', 1, 3, 3, 1),
(4, '2018-03-05', '2018-03-06', 'Active', 1, 4, 4, 1),
(5, '2018-03-05', '2018-03-06', 'Active', 1, 5, 5, 1),
(6, '2018-03-05', '2018-03-06', 'Active', 1, 6, 6, 1),
(7, '2018-03-05', '2018-03-06', 'Active', 1, 7, 7, 1),
(8, '2018-03-05', '2018-03-06', 'Active', 1, 8, 8, 1),
(9, '2018-03-05', '2018-03-06', 'Active', 1, 9, 9, 1),
(10, '2018-03-05', '2018-03-06', 'Active', 1, 10, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `floor` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `code`, `floor`, `number`, `status`, `branch_id`, `room_type_id`) VALUES
(1, '1001SU', 1, 1, 'Active', 1, 1),
(2, '1002SU', 1, 2, 'Active', 1, 1),
(3, '1003ED', 1, 3, 'Active', 1, 3),
(4, '1004ED', 1, 4, 'Active', 1, 3),
(5, '1005ED', 1, 5, 'Active', 1, 3),
(6, '1006ED', 1, 6, 'Active', 1, 3),
(7, '1007ED', 1, 7, 'Active', 1, 3),
(8, '1008DD', 1, 8, 'Active', 1, 2),
(9, '1009DD', 1, 9, 'Active', 1, 2),
(10, '1010DD', 1, 10, 'Active', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE `room_type` (
  `id` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `price_idr` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`id`, `capacity`, `code`, `description`, `name`, `status`, `price_idr`) VALUES
(1, 2, 'S', '22 meter persegi', 'Superior', 'Active', 700000),
(2, 2, 'DD', '24 meter persegi', 'Double Deluxe', 'Active', 700000),
(3, 2, 'ED', '36 meter persegi', 'Executive Deluxe', 'Active', 700000),
(4, 2, 'JS', '46 meter persegi', 'Junior Suite', 'Active', 700000);

-- --------------------------------------------------------

--
-- Table structure for table `season`
--

CREATE TABLE `season` (
  `id` int(11) NOT NULL,
  `create_date` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `end_date` date NOT NULL,
  `start_date` date NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `branch_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `season`
--

INSERT INTO `season` (`id`, `create_date`, `description`, `end_date`, `start_date`, `status`, `type`, `branch_id`) VALUES
(1, '2018-02-15', 'Season 1', '2018-02-10', '2018-01-10', 'Active', 'Normal', 1),
(2, '2018-02-15', 'Season 2', '2018-03-10', '2018-02-10', 'Active', 'Promo', 1),
(3, '2018-02-15', 'Season 3', '2018-04-10', '2018-03-10', 'Active', 'High', 1),
(4, '2018-02-15', 'Season 4', '2018-05-10', '2018-04-10', 'Active', 'Normal', 2),
(5, '2018-02-15', 'Season 5', '2018-06-10', '2018-05-10', 'Active', 'Promo', 1),
(6, '2018-02-15', 'Season 6', '2018-07-10', '2018-06-10', 'Active', 'High', 1),
(7, '2018-02-15', 'Season 7', '2018-08-10', '2018-07-10', 'Active', 'Normal', 1),
(8, '2018-02-15', 'Season 8', '2018-09-10', '2018-08-10', 'Active', 'Promo', 1),
(9, '2018-02-15', 'Season 9', '2018-10-10', '2018-09-10', 'Active', 'High', 1),
(10, '2018-02-15', 'Season 10', '2018-11-10', '2018-10-10', 'Active', 'Normal', 2);

-- --------------------------------------------------------

--
-- Table structure for table `special_demand`
--

CREATE TABLE `special_demand` (
  `id` int(11) NOT NULL,
  `demand` varchar(255) NOT NULL,
  `reservation_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `special_demand`
--

INSERT INTO `special_demand` (`id`, `demand`, `reservation_id`) VALUES
(1, '1 extra bed', 1),
(2, 'No smoking', 1),
(3, '1 extra bed', 2),
(4, 'No smoking', 2),
(5, '1 extra bed', 3),
(6, 'No smoking', 4),
(7, '1 extra bed', 5),
(8, 'No smoking', 5),
(9, '1 extra bed', 6),
(10, 'No smoking', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKcwcof6gi0txp7t7mow4ii4584` (`branch_id`);

--
-- Indexes for table `additional_facility`
--
ALTER TABLE `additional_facility`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1l9c5bvm91gaficr06gf67x6k` (`reservation_id`),
  ADD KEY `FKlhgka59x6wuc7lk01pnw5cheo` (`facility_id`);

--
-- Indexes for table `bed`
--
ALTER TABLE `bed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKdrb67qqamcxg547kq9gtf3mj0` (`room_type_id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_facility`
--
ALTER TABLE `detail_facility`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK8suvalasf9bjyjoq60fnrh1nv` (`facility_id`),
  ADD KEY `FKey0wya2dreorg5wuybj1e9y1v` (`room_type_id`);

--
-- Indexes for table `detail_reservation`
--
ALTER TABLE `detail_reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK4wm8exqplahyc03svsvg6xv` (`price_id`),
  ADD KEY `FK9rj63touy5nxhgyq7qg1oqok` (`reservation_id`),
  ADD KEY `FKfvd08bn6hlqdfvj71ni5tmkmn` (`room_type_id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_description`
--
ALTER TABLE `facility_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1yim6y8or4eh7kocoe55a3b7k` (`room_type_id`);

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK5j0tmyxmlit1helhv25savmpk` (`employee_id`),
  ADD KEY `FKl32lvvwyuy22y4vhteuafhsmy` (`reservasi_id`);

--
-- Indexes for table `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK90xjlkjc6ewest3si0gjnbo4e` (`season_id`),
  ADD KEY `FKgo4jls5h3rk6k1u6xpg1cul4k` (`room_type_id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK8rduaf1n8es4jf5wagbjhjepj` (`guest_id`),
  ADD KEY `FKislrpbuuvlhv0lpik4nt3w2y5` (`employee_id`),
  ADD KEY `FKlioab2ehkn3s2aaxm74912x1i` (`branch_id`);

--
-- Indexes for table `reserved_room`
--
ALTER TABLE `reserved_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK9candcgjnfv0tnk2l427k51rb` (`branch_id`),
  ADD KEY `FKp2gdpag79qoje8fo2ilofmuy6` (`room_id`),
  ADD KEY `FKpacbes1vedu66l9i80ebxta2o` (`room_type_id`),
  ADD KEY `FKq01lh0svrpkadmlbxjy4xb4h3` (`reservation_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_arevbfcloncxciyi0vbx1m4he` (`code`),
  ADD KEY `FK3mf32q0hwpnmu7rf58gjtjrmj` (`branch_id`),
  ADD KEY `FKd468eq7j1cbue8mk20qfrj5et` (`room_type_id`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKqxwntlqlfm36kc9m5gsr8cied` (`branch_id`);

--
-- Indexes for table `special_demand`
--
ALTER TABLE `special_demand`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKdtv89q5mas9be0wml399qe9i9` (`reservation_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `additional_facility`
--
ALTER TABLE `additional_facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `bed`
--
ALTER TABLE `bed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `detail_facility`
--
ALTER TABLE `detail_facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `detail_reservation`
--
ALTER TABLE `detail_reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `facility_description`
--
ALTER TABLE `facility_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `guest`
--
ALTER TABLE `guest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `price`
--
ALTER TABLE `price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `reserved_room`
--
ALTER TABLE `reserved_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `season`
--
ALTER TABLE `season`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `special_demand`
--
ALTER TABLE `special_demand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `FKcwcof6gi0txp7t7mow4ii4584` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`);

--
-- Constraints for table `additional_facility`
--
ALTER TABLE `additional_facility`
  ADD CONSTRAINT `FK1l9c5bvm91gaficr06gf67x6k` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`),
  ADD CONSTRAINT `FKlhgka59x6wuc7lk01pnw5cheo` FOREIGN KEY (`facility_id`) REFERENCES `facility` (`id`);

--
-- Constraints for table `bed`
--
ALTER TABLE `bed`
  ADD CONSTRAINT `FKdrb67qqamcxg547kq9gtf3mj0` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);

--
-- Constraints for table `detail_facility`
--
ALTER TABLE `detail_facility`
  ADD CONSTRAINT `FK8suvalasf9bjyjoq60fnrh1nv` FOREIGN KEY (`facility_id`) REFERENCES `facility` (`id`),
  ADD CONSTRAINT `FKey0wya2dreorg5wuybj1e9y1v` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);

--
-- Constraints for table `detail_reservation`
--
ALTER TABLE `detail_reservation`
  ADD CONSTRAINT `FK4wm8exqplahyc03svsvg6xv` FOREIGN KEY (`price_id`) REFERENCES `price` (`id`),
  ADD CONSTRAINT `FK9rj63touy5nxhgyq7qg1oqok` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`),
  ADD CONSTRAINT `FKfvd08bn6hlqdfvj71ni5tmkmn` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);

--
-- Constraints for table `facility_description`
--
ALTER TABLE `facility_description`
  ADD CONSTRAINT `FK1yim6y8or4eh7kocoe55a3b7k` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `FK5j0tmyxmlit1helhv25savmpk` FOREIGN KEY (`employee_id`) REFERENCES `account` (`id`),
  ADD CONSTRAINT `FKl32lvvwyuy22y4vhteuafhsmy` FOREIGN KEY (`reservasi_id`) REFERENCES `reservation` (`id`);

--
-- Constraints for table `price`
--
ALTER TABLE `price`
  ADD CONSTRAINT `FK90xjlkjc6ewest3si0gjnbo4e` FOREIGN KEY (`season_id`) REFERENCES `season` (`id`),
  ADD CONSTRAINT `FKgo4jls5h3rk6k1u6xpg1cul4k` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK8rduaf1n8es4jf5wagbjhjepj` FOREIGN KEY (`guest_id`) REFERENCES `guest` (`id`),
  ADD CONSTRAINT `FKislrpbuuvlhv0lpik4nt3w2y5` FOREIGN KEY (`employee_id`) REFERENCES `account` (`id`),
  ADD CONSTRAINT `FKlioab2ehkn3s2aaxm74912x1i` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`);

--
-- Constraints for table `reserved_room`
--
ALTER TABLE `reserved_room`
  ADD CONSTRAINT `FK9candcgjnfv0tnk2l427k51rb` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  ADD CONSTRAINT `FKp2gdpag79qoje8fo2ilofmuy6` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `FKpacbes1vedu66l9i80ebxta2o` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`),
  ADD CONSTRAINT `FKq01lh0svrpkadmlbxjy4xb4h3` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`);

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `FK3mf32q0hwpnmu7rf58gjtjrmj` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  ADD CONSTRAINT `FKd468eq7j1cbue8mk20qfrj5et` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);

--
-- Constraints for table `season`
--
ALTER TABLE `season`
  ADD CONSTRAINT `FKqxwntlqlfm36kc9m5gsr8cied` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`);

--
-- Constraints for table `special_demand`
--
ALTER TABLE `special_demand`
  ADD CONSTRAINT `FKdtv89q5mas9be0wml399qe9i9` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
